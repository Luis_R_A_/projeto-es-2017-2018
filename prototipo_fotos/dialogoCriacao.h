#ifndef C_DIALOGOCRIACAO_H
#define C_DIALOGOCRIACAO_H

#include <QDialog>
#include <QFrame>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <QComboBox>
#include <QString>
#include "../testFicheiros/album.h"
#include "../testFicheiros/page.h"
#include "../testFicheiros/photo.h"
#include "../testFicheiros/file.h"
//#include "mainWindow.h"

#define RETURN_SUCCESS 0
#define RETURN_ERROR_UNKNOWN 1
#define RETURN_ERROR_NAME_EXISTS 2
#define RETURN_ERROR_NOT_FOUND 3
#define RETURN_ERROR_COPY_FAILED 4
#define LEVEL_ALBUM 1
#define LEVEL_PAGE 2
#define LEVEL_PHOTO 3
#define LEVEL_PERSON 4
#define MODE_CREATE 1
#define MODE_EDIT 2
#define MODE_REMOVE 3

extern c_listAlbum *albumsList;
class c_mainWindow;
//void c_mainWindow::recieveNewData();
namespace Ui {
class c_dialogoCriacao;
}

class c_dialogoCriacao : public QDialog
{
    Q_OBJECT

public:
    c_listAlbum * albumList;
    bool in_use;
    explicit c_dialogoCriacao(QWidget *parent = 0);
    ~c_dialogoCriacao();

    int numAlbum;
    int numPage;
    int numFoto;
    //int lastAcessedLevel, actionMode, retornado;

    c_mainWindow * mainWindow;
    int lastAcessedLevel, actionMode, retornado, lastAlbumSelected;

    //void adicionaAlbum(QString nome){};
    c_noAlbum * albumTemp;
    c_listPage * pageListTemp;
    c_noPage * pageTemp;
    c_listPhoto * photoListTemp;
    c_noPhoto * photoTemp;
    QString fileName;
    void clearForm();
    void initializeList(c_listAlbum * _albumList, c_mainWindow * _mainWindow);
    void setDialogLevel(int _newLevel);
    void setDialogLevel(int _newLevel, int _newAction);
private slots:


    //void on_comboBox_currentIndexChanged(const QString &arg1);

    //void on_albumSelect_currentIndexChanged(int index);

    void on_finish_clicked();

    void on_cancel_clicked();

    void on_albumSelect_currentIndexChanged(int _index);

    void on_pageSelect_currentIndexChanged(int _index);

    void on_photoSelect_currentIndexChanged(int _index);

    //void on_albumLable_windowTitleChanged(const QString &title);

    void on_fotoButton_clicked();
signals:
    void haveNewData();

private:
    QString nivel[4], modo[4], action[4], buffer[1], retorno[5], novoNome;
    Ui::c_dialogoCriacao *ui;

    void geraResposta(int _returnCode);
};

#endif // C_DIALOGOCRIACAO_H
