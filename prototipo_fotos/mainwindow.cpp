#include <QWidget>
#include <QImageReader>
#include <QDebug>
#include "dialogoCriacao.h"
#include "mainWindow.h"
#include "ui_mainwindow.h"
/*
#define RETURN_SUCCESS 0
#define RETURN_DEFAUT_ERROR 1
#define LEVEL_ALBUM 1
#define LEVEL_PAGE 2
#define LEVEL_PHOTO 3
#define LEVEL_PERSON 4
#define MODE_CREATE 1
#define MODE_EDIT 2
#define MODE_REMOVE 3
*/
c_mainWindow::c_mainWindow(QWidget *parent, c_listAlbum &_albumList) :
    QMainWindow(parent),
    ui(new Ui::c_mainWindow)
{
    //d.setParent(this);
    _albumList.debugPrint();
    ui->setupUi(this);
    int i = 0;
    albumList = &_albumList;
    c_noAlbum *temp = _albumList.getAlbum(i);
    while(temp != NULL){
        ui->listWidget->addItem(temp->name);
        i++;
        temp = _albumList.getAlbum(i);

    }
    num_albuns = i;
    //d.initializeList(&_albumList, this);



    ui->listWidget_3->setViewMode(QListWidget::IconMode);

    ui->listWidget_3->setIconSize(QSize(100,100));

    ui->listWidget_3->setResizeMode(QListWidget::Adjust);

    albumList = &_albumList;
}

c_mainWindow::c_mainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::c_mainWindow)
{

    ui->setupUi(this);
   /* for(int i = 0; i < num_albuns; i++)
        {
            ui->listWidget->addItem(data[i][0].c_str());
            ui.a
        }
*/
    ui->listWidget_3->setViewMode(QListWidget::IconMode);

    ui->listWidget_3->setIconSize(QSize(100,100));

    ui->listWidget_3->setResizeMode(QListWidget::Adjust);
}

c_mainWindow::~c_mainWindow()
{
    delete ui;
}

/*
 * preenche as paginas do album atual
 */
void c_mainWindow::on_listWidget_currentRowChanged(int _currentRow)
{
    qDebug() <<"on_listWidget_currentItemChanged";
    ui->imagem->clear();
    ui->listWidget_2->clear();
    ui->listWidget_3->clear();
    //ui->listWidget_3->clear();

    album = _currentRow;
    if(album < 0)
        return;
    qDebug() <<"on_listWidget_currentItemChanged - album = " + QString::number(album);

    //d.lastAcessedLevel = LEVEL_ALBUM;
    //d.actionMode = MODE_CREATE;
    //d.lastAlbumSelected = album;

    c_noAlbum *temp = albumList->getAlbum(album);
    albumList->openAlbum(temp->name);
    int i = 0;
    c_noPage *temp2 = temp->listPages.getPage(i);
    while(temp2 != NULL){
        ui->listWidget_2->addItem(temp2->name);
        i++;
        temp2 = temp->listPages.getPage(i);
    }
    numPages = i;
    qDebug() <<"on_listWidget_currentItemChanged - numPages = " + QString::number(numPages);

}

/*
 * preenche as fotos da pagina atual
 */
void c_mainWindow::on_listWidget_2_currentRowChanged(int _currentRow)
{
    ui->imagem->clear();
    qDebug() <<"on_listWidget_2_currentItemChanged";
    album = ui->listWidget->currentRow();
    if(album < 0)
        return;
    qDebug() <<"on_listWidget_2_currentItemChanged - album = " + QString::number(album);
    c_noAlbum *temp = albumList->getAlbum(album);

    albumList->openAlbum(temp->name);

    //temp->listPages.getPage(i);



    //if(_current != _previous){};
    pagina = _currentRow/* + 1*/;
    if(pagina < 0)return;
    qDebug() <<"on_listWidget_2_currentItemChanged - page = " + QString::number(pagina);

    d.lastAcessedLevel = LEVEL_PAGE;
    d.actionMode = MODE_CREATE;


    c_noPage *temp2 = temp->listPages.getPage(pagina);

    temp->listPages.openPage(temp2->name);
    ui->listWidget_3->clear();

    QString base = "\\fotos\\";
    QString separador = "\\";
    QString estencao = ".jpg";
    QString nome;

    int i = 0;
    c_noPhoto *temp3 = temp2->listPhotos.getPhoto(i);
    //temp2->listPhotos.debugPrint();
    while(temp3 != NULL){
       // ui->listWidget_3->addItem(temp3->name);
        //qDebug() << temp3->filePath;

        ui->listWidget_3->addItem(new QListWidgetItem(QIcon(temp3->filePath), temp3->name));
        qDebug() << "foto adicionada:" << temp3->name;
        //ui->listWidget_3->addItem()
        i++;
        temp3 = temp2->listPhotos.getPhoto(i);
    }
    numFotos = i;
    qDebug() <<"on_listWidget_currentItemChanged - numFotos = " + QString::number(numFotos);


}

void c_mainWindow::on_listWidget_3_currentRowChanged(int _currentRow)
{
    ui->imagem->clear();
    QString base = "\\fotos\\";
    QString separador = "\\";
    QString estencao = ".jpg";
    QString endereco = (base + QString::number(album) + separador + QString::number(pagina) + separador + QString::number(_currentRow+1) + estencao);
    qDebug() << "foto: " <<endereco;
    //QGraphicsScene scene;
    //QPixmap pixmap(endereco.toLatin1());
    //scene.addPixmap(pixmap);

    ;;;c_noAlbum *temp = albumList->getAlbum(album);
    c_noPage *temp2 = temp->listPages.getPage(pagina);
    if(temp2 != NULL){c_noPhoto *temp3 = temp2->listPhotos.getPhoto(_currentRow);

    QImageReader reader(temp3->filePath);
    reader.setAutoTransform(true);
    image = reader.read();
    if (image.isNull()) {
        qDebug() << "nao foi encontrado:" << endereco;
    }
    }else{
        qDebug() << "no fotos";
    }

    //int wI = newImage.width(), wW = ui->imagem->width();
    //int hI = newImage.height(), hW = ui->imagem->height();
    //double wR = wW/wI, hR = hW/hI, iR;
    //iR = std::max(wR, hR);

    //ui->imagem->setScaledContents(true);
    ui->imagem->setPixmap(QPixmap::fromImage(image.scaled(ui->imagem->width()-1, ui->imagem->height()-1, Qt::KeepAspectRatio)));

}

void c_mainWindow::resizeEvent(QResizeEvent* event) {

    //this->QWidget::resizeEvent(event);
    //ui->imagem->setPixmap(QPixmap::fromImage(image.scaled(ui->imagem->width()-1,ui->imagem->height()-1, Qt::KeepAspectRatio)));

}

void c_mainWindow::on_botaoApagar_clicked(){
    d.close();
    d.setDialogLevel(LEVEL_ALBUM, MODE_REMOVE);
    d.initializeList(albumList, this);
    d.clearForm();
    d.in_use = true;
    d.show();
}

void c_mainWindow::on_botaoEditar_clicked()
{
    d.close();
    d.setDialogLevel(LEVEL_ALBUM, MODE_EDIT);
    d.initializeList(albumList, this);
    d.clearForm();
    d.in_use = true;
    d.show();
}

void c_mainWindow::on_botaoCriar_clicked()
{
    d.close();
    d.setDialogLevel(LEVEL_ALBUM, MODE_CREATE);
    d.initializeList(albumList, this);
    d.clearForm();
    d.in_use = true;
    d.show();
}

void c_mainWindow::recieveNewData(){
    ui->listWidget->clear();
    int i = 0;
    c_noAlbum *temp = albumList->getAlbum(i);
    while(temp != NULL){
        ui->listWidget->addItem(temp->name);
        i++;
        temp = albumList->getAlbum(i);

    }
    num_albuns = i;
    ui->listWidget_2->clear();
    ui->listWidget_3->clear();
    ui->listWidget_4->clear();
    ui->listWidget->setCurrentRow(0);

}
