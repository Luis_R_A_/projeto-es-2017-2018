#ifndef DIALOGOS_H
#define DIALOGOS_H

//#include <QtWidgets>
//#include <QTranslator>
//#include <QLocale>
//#include <QLibraryInfo>

QWizardPage *createAlbumCreation()
{
    QWizardPage *page = new QWizardPage;
    page->setTitle("New Album");
    page->setSubTitle("Please fill all fields.");

    QLabel *nameLabel = new QLabel("Nome:");
    QLineEdit *nameLineEdit = new QLineEdit;

    QLabel *descriptionLabel = new QLabel("Descrição:");
    QLineEdit *descriptionLabelEdit = new QLineEdit;

    QGridLayout *layout = new QGridLayout;
    layout->addWidget(nameLabel, 0, 0);
    layout->addWidget(nameLineEdit, 0, 1);
    layout->addWidget(descriptionLabel, 1, 0);
    layout->addWidget(descriptionLabelEdit, 1, 1);
    page->setLayout(layout);

    page->registerField("nameLineEdit*", nameLineEdit);
    page->registerField("descriptionLabelEdit*", descriptionLabelEdit);
    return page;
}

#endif // DIALOGOS_H
