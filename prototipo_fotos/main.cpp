#include "mainWindow.h"
#include <QtWidgets>
#include <QTranslator>
#include <QLocale>
#include <QLibraryInfo>
#include <QApplication>

#include "../testFicheiros/album.h"
#include <QDebug>

c_listAlbum albumList;
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    /*if(albumList.create("Album1 Luís àú123", "Test Album create", "I dunno") < 0)
        qDebug() << "Error creating: " << albumList.getError();
    if(albumList.create("Album2", "Test Album create 2", "I still dunno") <0)
        qDebug() << "Error creating: " << albumList.getError();
    if(albumList.create("Album3", "Test Album create 3", "I still dunno 2") <0)
        qDebug() << "Error creating: " << albumList.getError();
    if(albumList.create("Album4", "Test Album create 4", "I still dunno 3") <0)
        qDebug() << "Error creating: " << albumList.getError();
    if(albumList.create("Album5", "Test Album create 5", "I still dunno 4") <0)
        qDebug() << "Error creating: " << albumList.getError();
    if(albumList.create("Album1", "Test Album create 5", "I still dunno 4") <0)
        qDebug() << "Error creating: " << albumList.getError();
    if(albumList.create("Album Luís!!", "Test Album create", "I dunno") < 0)
        qDebug() << "Error creating: " << albumList.getError();
    albumList.debugPrint();
    return 0;*/

    if(albumList.loadList() < 0)
        qDebug() << "Error loading: " << albumList.getError();
    //albumList.debugPrint();
    //MainWindow w;
    c_mainWindow w(NULL,albumList);
    qDebug() << "directorio:" << QDir::currentPath();
        w.show();

    return a.exec();
}
