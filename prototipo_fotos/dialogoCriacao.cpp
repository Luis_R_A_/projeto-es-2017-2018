#include "dialogoCriacao.h"
#include "ui_dialogoCriacao.h"
#include "mainwindow.h"
#include <QMessageBox>
#include <QComboBox>
#include <Qdebug.h>
#include <QFileDialog>
#include <QDebug>
#include <QImageReader>
/*
#define RETURN_SUCCESS 0
#define RETURN_ERROR_UNKNOWN 1
#define RETURN_ERROR_NAME_EXISTS 2
#define RETURN_ERROR_NOT_FOUND 3
#define LEVEL_ALBUM 1
#define LEVEL_PAGE 2
#define LEVEL_PHOTO 3
#define LEVEL_PERSON 4
#define MODE_CREATE 1
#define MODE_EDIT 2
#define MODE_REMOVE 3
*/
void c_dialogoCriacao::setDialogLevel(int _newLevel){
//funsão para mostrar e ou esconder os diversos elementos do dialogo de criação
    qDebug() << "setDialogLevel - _newLevel: " << _newLevel;
    lastAcessedLevel=_newLevel;

    if((actionMode == MODE_CREATE)&&(_newLevel == LEVEL_PAGE))
        ui->pageSelect2->setVisible(true);
    else
        ui->pageSelect2->setVisible(false);

qDebug()<<"here";
    ui->pageLable->setVisible(true);
    ui->pageSelect->setVisible(true);
    ui->fotoLable->setVisible(true);
    ui->fotoSelect->setVisible(true);

    ui->personLable->setVisible(true);
    ui->personSelect->setVisible(true);

    if(actionMode != MODE_REMOVE){
        ui->photoPathLable->setVisible(true);
        ui->fotoPath->setVisible(true);
        ui->fotoButton->setVisible(true);
        ui->descriptionLable->setVisible(true);
        ui->descriptionText->setVisible(true);
        ui->nameBox->setVisible(true);
        ui->nameLable->setVisible(true);
    }else{
        ui->nameBox->setVisible(false);
        ui->nameLable->setVisible(false);
    }

    switch (_newLevel) {
    case LEVEL_ALBUM :{
        qDebug() << " album";
        ui->pageLable->setVisible(false);
        ui->pageSelect->setVisible(false);
        ui->descriptionLable->setVisible(false);
        ui->descriptionText->setVisible(false);
    }
    case LEVEL_PAGE :  {
        qDebug() << " page";
        ui->fotoLable->setVisible(false);
        ui->fotoSelect->setVisible(false);
        ui->photoPathLable->setVisible(false);
        ui->fotoPath->setVisible(false);
        ui->fotoButton->setVisible(false);
    }
    case LEVEL_PHOTO :{
        qDebug() << " foto";
        ui->personLable->setVisible(false);
        ui->personSelect->setVisible(false);
    }
    case LEVEL_PERSON :
        qDebug() << " person";

        break;
    default:
        qDebug() << "setDialogLevel - newLevel has invalid value of: " << _newLevel << endl;
    }
}

void c_dialogoCriacao::setDialogLevel(int _newLevel, int _newAction){
    actionMode = _newAction;
    qDebug() << "setDialogLevel - _newAction: " << actionMode;
    setDialogLevel(_newLevel);
}

//c_listAlbum *albumsList;
c_dialogoCriacao::c_dialogoCriacao(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::c_dialogoCriacao)
{
    lastAcessedLevel = LEVEL_ALBUM;
    actionMode = MODE_CREATE;//modo por defeito
    retornado = -1;
    nivel[LEVEL_ALBUM] = "do album ";
    nivel[LEVEL_PAGE] = "da pagina ";
    nivel[LEVEL_PHOTO] = "da foto ";
    novoNome = "<nome do album>";
    modo[MODE_CREATE] = "A criação ";
    modo[MODE_EDIT] = " A modificação ";
    modo[MODE_REMOVE] = "A remoção ";
    action[MODE_CREATE] = "criar";
    action[MODE_EDIT] = "editar";
    action[MODE_REMOVE] = "apagar";
    buffer[0] = " falhou catastroficamente"; //no caso a função retornar -1;
    retorno[RETURN_SUCCESS] = " foi bem sucedida";
    retorno[RETURN_ERROR_UNKNOWN] = " falhou";
    retorno[RETURN_ERROR_NAME_EXISTS] = "\n nao susbtituiu o ficheiro existente";
    retorno[RETURN_ERROR_NOT_FOUND] = "\n nao encontrou o ficheiro";
    retorno[RETURN_ERROR_COPY_FAILED] = "\n nao encontrou o ficheiro";

    ui->setupUi(this);

    ui->pageSelect2->addItem("Festa");
    ui->pageSelect2->addItem("Coisa/Pessoa");
    ui->pageSelect2->addItem("Viagem");
    ui->pageSelect2->addItem("Outros");

    setDialogLevel(LEVEL_ALBUM);
    /*ui->pageLable->setVisible(false);
    ui->pageSelect->setVisible(false);
    ui->fotoLable->setVisible(false);
    ui->fotoSelect->setVisible(false);
    ui->fotoPath->setVisible(false);
    ui->descriptionLable->setVisible(false);
    ui->descriptionText->setVisible(false);*/
}

c_dialogoCriacao::~c_dialogoCriacao()
{
    delete ui;
}

/*void c_dialogoCriacao::on_comboBox_currentIndexChanged(const QString &arg1)
{
    if(ui->albumSelect->currentIndex() == 1){
        //ui->photoSelect->activated(1);
    }
}*/

void c_dialogoCriacao::on_finish_clicked()
{
    retornado = 1;
    QMessageBox msgBox;
    QMessageBox confirmRemove;
    confirmRemove.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    confirmRemove.setDefaultButton(QMessageBox::No);
    if(this->actionMode != MODE_CREATE)this->lastAcessedLevel--;
    //if((this->actionMode == MODE_REMOVE))
    switch(this->lastAcessedLevel){
    case(LEVEL_ALBUM):{
        qDebug() << "on_finish_clicked - LEVEL_ALBUM";
        switch(this->actionMode){
        case(MODE_CREATE):{
            qDebug() << "on_finish_clicked - MODE_CREATE";
            //chama função
            retornado = albumList->create(ui->nameBox->text() , ui->descriptionText->toPlainText(), "");

            break;
        }case(MODE_EDIT):{
            //chama função
            break;
        }case(MODE_REMOVE):{
            QString aux = "tem a certeza que deseija remover o album\n" + albumTemp->name + "\ne toda a sua informação?";
            confirmRemove.setText(aux);
            int ret = confirmRemove.exec();
            qDebug() << ret;
            //chama função
            break;
        }default:{
            break;}
        }/*=== END OF SWITCH this->actionMode ===*/
        break;
    }/*=== END OF CASE LEVEL_ALBUM ===*/
    case(LEVEL_PAGE):{
        qDebug() << "on_finish_clicked - LEVEL_PAGE";
        switch(this->actionMode){
        case(MODE_CREATE):{
            qDebug() << "on_finish_clicked - MODE_CREATE";
            c_noAlbum *temp = (albumList->getAlbum(ui->albumSelect->currentIndex()-1));
            c_date date;
            retornado = temp->listPages.create(ui->pageSelect2->currentText(), ui->nameBox->text() , ui->descriptionText->toPlainText(), date, "Aniversário");

            break;
        }case(MODE_EDIT):{
            //chama função
            break;
        }case(MODE_REMOVE):{
            QString aux = "tem a certeza que deseija remover a pagina\n" + pageTemp->name + "\ne toda a sua informação?";
            confirmRemove.setText(aux);
            int ret = confirmRemove.exec();
            qDebug() << ret;
            //chama função
            break;
        }default:{
            break;}
        }/*=== END OF SWITCH this->actionMode ===*/
        break;
    }/*=== END OF CASE LEVEL_PAGE ===*/
    case(LEVEL_PHOTO):{
        switch(this->actionMode){
        case(MODE_CREATE):{
            c_noAlbum *temp = (albumList->getAlbum(ui->albumSelect->currentIndex()-1));
            c_noPage *temp2 = temp->listPages.getPage((ui->pageSelect->currentIndex()-1));
            c_date date;
            s_resolution tempResolution;
            QImageReader reader(ui->fotoPath->text());
            QImage newImage = reader.read();
            tempResolution.x = newImage.width();
            tempResolution.y = newImage.height();
            QFile f(ui->fotoPath->text());
            QString str = ui->nameBox->text() +"."+ ui->fotoPath->text().section(".", -1, -1);
            c_file fileTemp;
            bool copySuccess = QFile::copy(f.fileName(), fileTemp.convertString(temp->name)+"/"+fileTemp.convertString(temp2->name)+"/"+str);
            if(!copySuccess)
                retornado = -4;
            else retornado = temp2->listPhotos.create(tempResolution, date, str);
            break;
        }case(MODE_EDIT):{
            //chama função
            break;
        }case(MODE_REMOVE):{
            QString aux = "tem a certeza que deseija remover a foto\n" + photoTemp->name + "\ne toda a sua informação?";
            confirmRemove.setText(aux);
            int ret = confirmRemove.exec();
            qDebug() << ret;
            //chama função
            break;
        }default:{
            break;}
        }/*=== END OF SWITCH this->actionMode ===*/
        break;
    }/*=== END OF CASE LEVEL_PHOTO ===*/
    default:{

       break;
    }
    }/*=== END OF SWITCH this->lastAcessedLevel ===*/
    msgBox.setText(modo[this->actionMode] + nivel[this->lastAcessedLevel] + ui->nameBox->text() + retorno[-1*this->retornado]);
    msgBox.setIcon(QMessageBox::Warning);
    if(retornado==RETURN_SUCCESS){
        this->close();
        emit haveNewData();
        qDebug() << "deve ter actualizado";
    }
    msgBox.exec();
}

void c_dialogoCriacao::on_cancel_clicked()
{
    this->close();
}

void c_dialogoCriacao::on_albumSelect_currentIndexChanged(int _index)
{
    qDebug() << "on_albumSelect_currentIndexChanged";
    ui->fotoSelect->clear();
    ui->personSelect->clear();
    ui->pageSelect->clear();
    //ui->pageSelect->setCurrentIndex(0);

    //
    if(_index < 0)
        return;
    qDebug() <<"on_albumSelect_currentIndexChanged index = " + _index;


    if(_index == 0){
        setDialogLevel(LEVEL_ALBUM);
    }else{
        if(this->actionMode != MODE_CREATE){
            ui->pageSelect->addItem("<<o album>>");
        }else{
            ui->pageSelect->addItem("<<Nova Pagina>>");
        }

            _index--;
            numAlbum = _index;
            numPage = 0;
            numFoto = 0;
            albumTemp = albumList->getAlbum(numAlbum);
            albumList->openAlbum(albumTemp->name);

            pageListTemp = &(albumTemp->listPages);
            //pageTemp = pageListTemp->getPage(i);

            int i = 0;
            pageTemp = pageListTemp->getPage(i);
            while(pageTemp != NULL){
                ui->pageSelect->addItem(pageTemp->name);
                i++;
                pageTemp = pageListTemp->getPage(i);
            }
            setDialogLevel(LEVEL_PAGE);
    }
}

void c_dialogoCriacao::on_pageSelect_currentIndexChanged(int _index)
{
    qDebug() << "on_albumSelect_currentIndexChanged";
    ui->personSelect->clear();
    ui->fotoSelect->clear();
    //ui->fotoSelect->setCurrentIndex(0);

    //
    if(_index < 0)
        return;
    qDebug() <<"on_pageSelect_currentIndexChanged index = " + _index;

    //ui->fotoSelect->clear();
    if(_index == 0){
        setDialogLevel(LEVEL_PAGE);
    }else{
        if(this->actionMode != MODE_CREATE){
            ui->fotoSelect->addItem("<<a Pagina>>");
        }else{
            ui->fotoSelect->addItem("<<Nova Foto>>");
        }
        _index--;
        //numAlbum = numAlbum;
        numPage = _index;
        numFoto = 0;
        //albumTemp = albumList->getAlbum(numAlbum);
        //albumList->openAlbum(albumTemp->name);

        pageListTemp = &(albumTemp->listPages);
        pageTemp = pageListTemp->getPage(_index);
        pageListTemp->openPage(pageTemp->name);
        photoListTemp = &(pageTemp->listPhotos);
        //ui->fotoSelect->addItem("<<Nova Foto>>");

        int i = 0;
        photoTemp = photoListTemp->getPhoto(i);
        //if(photoTemp!=NULL)QString aux = photoTemp->name;
        while(photoTemp != NULL){
            ui->fotoSelect->addItem(photoTemp->name);
            i++;
            photoTemp = photoListTemp->getPhoto(i);
        }


        setDialogLevel(LEVEL_PHOTO);
    }
}

void c_dialogoCriacao::on_photoSelect_currentIndexChanged(int _index)
{
    if(_index < 0)
        return;
    qDebug() <<"on_photoSelect_currentIndexChanged index = " + _index;
    if(_index == 0){
        setDialogLevel(LEVEL_PHOTO);
    }else{
        if(this->actionMode != MODE_CREATE){
            ui->personSelect->addItem("<<a Foto>>");
        }else{
            ui->personSelect->addItem("<<Nova Pessoa>>");
        }
        setDialogLevel(LEVEL_PERSON);
        //lista todas as pessoas associadas á foto
    }
}

void c_dialogoCriacao::clearForm(){
    ui->albumSelect->setCurrentIndex(0);
    ui->pageSelect->setCurrentIndex(0);
    ui->fotoSelect->setCurrentIndex(0);
    ui->personSelect->setCurrentIndex(0);
    ui->personSelect->clear();
    ui->fotoSelect->clear();
    ui->pageSelect->clear();
    //ui->albumSelect->clear();
}

void c_dialogoCriacao::initializeList(c_listAlbum * _albumList, c_mainWindow * _mainWindow){
    ui->finish->setText(action[this->actionMode]);
    ui->albumSelect->clear();
    albumList = _albumList;
    ui->albumSelect->addItem("<<novo album>");
    int i = 0;
    albumTemp = _albumList->getAlbum(i);
    while(albumTemp != NULL){
        ui->albumSelect->addItem(albumTemp->name);
        i++;
        albumTemp = _albumList->getAlbum(i);

    }
    QObject::connect(this, SIGNAL(haveNewData()), _mainWindow,  SLOT(recieveNewData()));
}

void c_dialogoCriacao::on_fotoButton_clicked()
{
    QStringList mimeTypeFilters;
    mimeTypeFilters << "image/jpeg" // will show "JPEG image (*.jpeg *.jpg *.jpe)
                << "image/png"  // will show "PNG image (*.png)"
                << "application/octet-stream"; // will show "All files (*)"

    QFileDialog fileSelect(this);
    fileSelect.setMimeTypeFilters(mimeTypeFilters);
    //fileSelect.exec();
    fileName = fileSelect.getOpenFileName(this, tr("Open Image"), "../", tr("Image Files (*.png *.jpg *.bmp)"));

    ui->fotoPath->insert(fileName);
}
