#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QListWidget>
#include <QFrame>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <QComboBox>
#include <QMainWindow>
#include "dialogoCriacao.h"

#define RETURN_SUCCESS 0
#define RETURN_DEFAUT_ERROR 1
#define LEVEL_ALBUM 1
#define LEVEL_PAGE 2
#define LEVEL_PHOTO 3
#define LEVEL_PERSON 4
#define MODE_CREATE 1
#define MODE_EDIT 2
#define MODE_REMOVE 3

namespace Ui {
class c_mainWindow;
}

class c_mainWindow : public QMainWindow
{
    Q_OBJECT

public:
    c_dialogoCriacao d;//(this);
    c_listAlbum * albumList;
    int album;
    int pagina;
    int num_albuns;
    int numPages;
    int numFotos;
    double scaleFactor;
    std::string tags_pagina[10][10];
    std::string data[10][10];
    QImage image;
    explicit c_mainWindow(QWidget *parent = 0);
    c_mainWindow(QWidget *parent, c_listAlbum &_albumList);
    ~c_mainWindow();

private slots:
    void on_listWidget_currentRowChanged(int _currentRow);

    void on_listWidget_2_currentRowChanged(int _currentRow);

    void on_listWidget_3_currentRowChanged(int _currentRow);

    void resizeEvent(QResizeEvent* event);

    void on_botaoApagar_clicked();

    void on_botaoEditar_clicked();

    void on_botaoCriar_clicked();

public slots:
    void recieveNewData();
private:
    Ui::c_mainWindow *ui;
};

#endif // MAINWINDOW_H
