var annotated_dup =
[
    [ "c_date", "classc__date.html", "classc__date" ],
    [ "c_dialogoCriacao", "classc__dialogo_criacao.html", "classc__dialogo_criacao" ],
    [ "c_file", "classc__file.html", "classc__file" ],
    [ "c_listAlbum", "classc__list_album.html", "classc__list_album" ],
    [ "c_listPage", "classc__list_page.html", "classc__list_page" ],
    [ "c_listPeople", "classc__list_people.html", "classc__list_people" ],
    [ "c_listPhoto", "classc__list_photo.html", "classc__list_photo" ],
    [ "c_mainWindow", "classc__main_window.html", "classc__main_window" ],
    [ "c_noAlbum", "classc__no_album.html", "classc__no_album" ],
    [ "c_noPage", "classc__no_page.html", "classc__no_page" ],
    [ "c_noPeople", "classc__no_people.html", "classc__no_people" ],
    [ "c_noPhoto", "classc__no_photo.html", "classc__no_photo" ],
    [ "image", "classimage.html", "classimage" ],
    [ "s_resolution", "structs__resolution.html", "structs__resolution" ]
];