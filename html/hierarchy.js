var hierarchy =
[
    [ "c_date", "classc__date.html", null ],
    [ "c_file", "classc__file.html", null ],
    [ "c_listAlbum", "classc__list_album.html", null ],
    [ "c_listPage", "classc__list_page.html", null ],
    [ "c_listPeople", "classc__list_people.html", null ],
    [ "c_listPhoto", "classc__list_photo.html", null ],
    [ "c_noAlbum", "classc__no_album.html", null ],
    [ "c_noPage", "classc__no_page.html", null ],
    [ "c_noPeople", "classc__no_people.html", null ],
    [ "c_noPhoto", "classc__no_photo.html", null ],
    [ "image", "classimage.html", null ],
    [ "QDialog", null, [
      [ "c_dialogoCriacao", "classc__dialogo_criacao.html", null ]
    ] ],
    [ "QMainWindow", null, [
      [ "c_mainWindow", "classc__main_window.html", null ]
    ] ],
    [ "s_resolution", "structs__resolution.html", null ]
];