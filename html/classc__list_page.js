var classc__list_page =
[
    [ "c_listPage", "classc__list_page.html#a1c024a73e9d84d45df3e190ac9d3a57e", null ],
    [ "add", "classc__list_page.html#a080edbccfe9b4337c12a9f5b19e529f6", null ],
    [ "checkIfExists", "classc__list_page.html#add1e18f6110ed1c9b58565ffe70d68a3", null ],
    [ "create", "classc__list_page.html#a6c37df2e42ab75c19489373b3e3961af", null ],
    [ "create", "classc__list_page.html#a9d6871804dec16184e6a09e73ef60cb9", null ],
    [ "create", "classc__list_page.html#ab374ac1ba3521c381a1329c87928aff0", null ],
    [ "debugPrint", "classc__list_page.html#ad5c115e9aa60006febab3c46c78f106c", null ],
    [ "getPage", "classc__list_page.html#aacd373d00fe7f24ca25d4b28eadeea3c", null ],
    [ "getPage", "classc__list_page.html#a1d9d09f4aee17b7a78443c398c41bc43", null ],
    [ "loadList", "classc__list_page.html#a4851b2b7f93230e5a025600f973e1bb6", null ],
    [ "openPage", "classc__list_page.html#a6dc234e4482489e1dcd40b21e92d861c", null ],
    [ "remove", "classc__list_page.html#a471f7320eceef5c8fda94e3776482bd5", null ],
    [ "removeAll", "classc__list_page.html#aa64ba0a1b93c1826929c4ca52191efc0", null ],
    [ "search", "classc__list_page.html#ab57dd0c531a4e50366b3763732511b3b", null ],
    [ "setElementIndex", "classc__list_page.html#a17f053f2eb93a5770ea75de8cd70fc44", null ],
    [ "albumName", "classc__list_page.html#a59802984e217fb33ad000f7f06b6445a", null ],
    [ "head", "classc__list_page.html#a7e20c08eb884b6d1c6496b1c987a99af", null ],
    [ "validTypes", "classc__list_page.html#af951da4514587ea32e01a0d77de13786", null ]
];