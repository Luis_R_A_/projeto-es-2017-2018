var classc__list_people =
[
    [ "add", "classc__list_people.html#a490c1b4a84a8e20441ca8d00dca90951", null ],
    [ "create", "classc__list_people.html#a2b27f8cb40229bc912b730d096e5aac8", null ],
    [ "debugPrint", "classc__list_people.html#a924ed381ff323e4c968442ecdb080f69", null ],
    [ "getError", "classc__list_people.html#ad197b0f47b9efffa15693422a72ab782", null ],
    [ "loadList", "classc__list_people.html#a34c92bf00d8b6f0ff907b27d3e1ff9c6", null ],
    [ "modifyName", "classc__list_people.html#a3e6971963e10ad363b16e6ae6f231c50", null ],
    [ "remove", "classc__list_people.html#af2b9ae2ab6c733a43d324a397d4d77a4", null ],
    [ "removeall", "classc__list_people.html#a5ead4d189426a9221b08c321cd2235c1", null ],
    [ "search", "classc__list_people.html#ad73a44e6fe18d3c4c16c32ce7e4a2394", null ],
    [ "errorString", "classc__list_people.html#add174add6875b267e3837234cf3e3d8d", null ],
    [ "head", "classc__list_people.html#ac4a559713852edb9288a020ee7406ad0", null ],
    [ "name", "classc__list_people.html#a6de86ae87e5a069989b4685036af8534", null ]
];