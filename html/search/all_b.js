var searchData=
[
  ['s_5fresolution',['s_resolution',['../structs__resolution.html',1,'']]],
  ['savelist',['saveList',['../classc__file.html#ab02ac68e8d2fa1aa47002a6b4f703a1f',1,'c_file::saveList(c_noAlbum *_head)'],['../classc__file.html#a94d7b13148c5d241a37482cca84cdc9c',1,'c_file::saveList(c_listPage *_list)'],['../classc__file.html#a2106aa03201bacebb7b6f4a0764b2b01',1,'c_file::saveList(c_listPhoto *_list)'],['../classc__file.html#a7d028275033b4f862d89212587475673',1,'c_file::saveList(c_listPeople *_list)']]],
  ['search',['search',['../classc__list_album.html#a96aa56575621d8f7dce05c86a8b1363c',1,'c_listAlbum::search()'],['../classc__list_page.html#ab57dd0c531a4e50366b3763732511b3b',1,'c_listPage::search()'],['../classc__list_people.html#ad73a44e6fe18d3c4c16c32ce7e4a2394',1,'c_listPeople::search()']]],
  ['setelementindex',['setElementIndex',['../classc__list_album.html#ab472c34a59a495c847e9244d6124bb76',1,'c_listAlbum::setElementIndex()'],['../classc__list_page.html#a17f053f2eb93a5770ea75de8cd70fc44',1,'c_listPage::setElementIndex()'],['../classc__list_photo.html#a66c727cf92629aeb0ed4f1e6d813ec22',1,'c_listPhoto::setElementIndex()']]]
];
