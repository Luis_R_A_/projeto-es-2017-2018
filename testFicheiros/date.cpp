/*!
    \file
    \brief cpp file for class c_date

    See the header date.h for more info

*/

#include "date.h"


/*!
 * \brief c_date::c_date
 */
c_date::c_date(){
    day = 0;
    month = 0;
    year = 0;
}

/*!
 * \brief c_noAlbum::operator =, changes this values. The return really isn't anything.
 * \param _date2
 * \return Nothing
 */
c_date c_date::operator=(c_date _date2){

    c_date temp;
    temp.day = _date2.day;
    temp.month = _date2.month;
    temp.year = _date2.year;
    return temp;
}
