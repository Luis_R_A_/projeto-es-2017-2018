#ifndef PAGE_H
#define PAGE_H

#include "date.h"
#include "photo.h"
#include <string>

/*
    \defgroup Page_Package Page
    @{
    \brief Class for managing page lists
*/
/*!
    \ingroup Gestao_Package
	\addtogroup Gestao_Package
    @{
*/

class c_noPage{
public:
    /* Common to all */
     QString name;
     QString folderPath;
     QString type;

    /* for party */
     c_date date;
     QString partyType;
     QString description;

    /* for person or stuff */
    // QString description;

    /* for travel*/
    // QString description;
    c_date startDate;
    c_date endDate;

    /* for other */
    // QString description;
    //c_date startDate;
    //c_date endDate;

    c_listPhoto listPhotos;
    c_noPage *next;

    c_noPage();

    c_noPage operator=(c_noPage _elementToAdd);
    int debugPrint();
};


class c_listPage{
private:

    int elementIndex = 0;
    int lastError;
public:
    QString validTypes [4] = {"Party", "People/Stuff", "Travel", "Other"};
    c_noPage *head;
    QString albumName;

    c_listPage();
    //~c_listPage();

    int loadList();
    //for party
    int create( QString _type, QString _name, QString _description, c_date _date, QString _partyType);
    //for person or stuff
    int create( QString _type, QString _name, QString _description);
    //for travel or other
    int create( QString _type, QString _name, QString _description, c_date _startDate, c_date _endDate);

    int remove(QString _name);
    int removeAll();

    c_noPage* search(QString _name);
    int checkIfExists(QString _name);


    int debugPrint();

    int add(c_noPage _elementToAdd);

    c_noPage* openPage(QString _name);

    c_noPage* getPage(int _n);
    void setElementIndex(int _n);
    c_noPage* getPage();

};

/*!
 * @}
 */

#endif // PAGE_H
