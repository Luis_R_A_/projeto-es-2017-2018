/*!
    \file
    \brief cpp file for module Page

    See the header page.h for more info

*/

#include "page.h"
#include "file.h"
#include <QDebug>


/*================================================
 * c_noPage methods
 ================================================*/
/*!
 * \brief c_noPage::c_noPage
 */
c_noPage::c_noPage(){
    next = NULL;
}

/*!
 * \brief c_noPage::debugPrint, prints this node atributes
 * \return  0
 */
int c_noPage::debugPrint(){

    qDebug()  << "Name: " << name << " ; folder path: " << folderPath << " ; type: " << type
              << " ; partyType: " << partyType << " ; description: " << description
              << " ; date: " << date.day << '/' << date.month << '/' << date.year
              << " ; StartDate: " << startDate.day << '/' << startDate.month << '/' << startDate.year
              << " ; EndDate: " << endDate.day << '/' << endDate.month << '/' << endDate.year
              ;//<< '\n';
    return 0;
}

/*================================================
 * c_listPage methods
 ================================================*/

/*!
 * \brief c_listPage::c_listPage
 */
c_listPage::c_listPage(){
    head = NULL;
}

/*!
 * \brief c_listPage::loadList, loads a list of pages from a file
 * \return 0 if sucess, -1 otherwise
 */
int c_listPage::loadList(){
    c_file file;
    int error = file.loadList(*this);
    if(error < 0)
        return -1;
    return 0;
}
//int c_listPage::create( QString _type, QString _name, QString _description, c_date _date, QString _partyType){
//    if(head == NULL){
//        head = new(c_noPage);

//        head->name = _name;
//        head->description = _description;
//        head->party = _pageTypes;
//        //QString folderName = convertString(_name);
//        //head->folderName = folderName;

//        head->next = NULL;
//        //head->debugPrint();
//        /*if( createFolder(head) < 0)
//            return ERROR_UNKNOWN;*/
//    }
//    else{
//        /*if(checkIfExists(_name)){
//            lastError = ERROR_NAME_EXISTS;
//            return lastError;
//        }*/
//        c_noPage *temp = head;

//        while(temp->next != NULL){
//            temp = temp->next;
//        }

//        temp->next = new(c_noPage);
//        temp =  temp->next;
//        /* Set pointers to NULL */
//        //head->next = NULL;
//        //head->pagesHead = NULL;

//        temp->name = _name;
//        temp->description = _description;
//        //temp->pageTypes = _pageTypes;

//        //QString folderName = convertString(_name);
//        //temp->folderName = folderName;

//        temp->next = NULL;
//        //temp->debugPrint();
//        /*if( createFolder(temp) < 0)
//            return ERROR_UNKNOWN;*/
//    }



//    /*c_file testFile;
//    testFile.saveList(head);*/
//}

/*!
 * \brief c_listPage::create, creates a new page to add to the list. Suposed to be used for party type
 * \param _type - should be "Party"
 * \param _name - name of the page
 * \param _description - description of the page
 * \param _date - date of the event
 * \param _partyType - type of party
 * \return 0 if no errors occur. -1 otherwise
 */
int c_listPage::create( QString _type, QString _name, QString _description, c_date _date, QString _partyType){
    if(head == NULL){
        head = new(c_noPage);

        head->name = _name;
        head->type = _type;
        head->description=_description;
        head->date=_date;
        head->partyType=_partyType;

        head->next = NULL;

        c_file file;
        QString albumFolder = file.convertString(albumName);
        QString pageFolder = file.convertString(_name);
        QString path = albumFolder+'/'+pageFolder;
        head->folderPath = path;
        head->listPhotos.pagePath = path;
        head->listPhotos.pageName = head->name;
        if(file.createFolder(path) <0)
            return -1;
        //head->debugPrint();
    }
    else{
        if(checkIfExists(_name)){
            lastError = -1;
            return lastError;
        }
        c_noPage *temp = head;

        while(temp->next != NULL){
            temp = temp->next;
        }

        temp->next = new(c_noPage);
        temp =  temp->next;

        temp->name = _name;
        temp->type = _type;
        temp->description=_description;
        temp->date=_date;
        temp->partyType=_partyType;

        c_file file;
        QString albumFolder = file.convertString(albumName);
        QString pageFolder = file.convertString(_name);
        QString path = albumFolder+'/'+pageFolder;
        temp->folderPath = path;
        temp->listPhotos.pagePath = path;
        temp->listPhotos.pageName = temp->name;
        temp->next = NULL;
        //temp->listPages.albumName = folderName;
        //temp->debugPrint();

        if(file.createFolder(path) <0)
            return -1;
        /*if( createFolder(temp) < 0)
            return ERROR_UNKNOWN;*/
        //temp->debugPrint();
    }

    c_file testFile;
    testFile.saveList(this);

    return 0;
}

/*!
 * \brief c_listPage::create , creates a new page to add to the list. Suposed to be used for Person/Stuff
 * \param _type - should be "Person/Stuff"
 * \param _name - name of the person or thing
 * \param _description - description of the page
 * \return 0 if no errors occur. -1 otherwise
 */
int c_listPage::create( QString _type, QString _name, QString _description){
    if(head == NULL){
        head = new(c_noPage);

        head->name = _name;
        head->type = _type;
        head->description=_description;

        head->next = NULL;

        c_file file;
        QString albumFolder = file.convertString(albumName);
        QString pageFolder = file.convertString(_name);
        QString path = albumFolder+'/'+pageFolder;
        head->folderPath = path;
        head->listPhotos.pagePath = path;
        head->listPhotos.pageName = head->name;
        if(file.createFolder(path) <0)
            return -1;
        //head->debugPrint();
    }
    else{
        if(checkIfExists(_name)){
            lastError = -1;
            return lastError;
        }
        c_noPage *temp = head;

        while(temp->next != NULL){
            temp = temp->next;
        }

        temp->next = new(c_noPage);
        temp =  temp->next;

        temp->name = _name;
        temp->type = _type;
        temp->description=_description;

        c_file file;
        QString albumFolder = file.convertString(albumName);
        QString pageFolder = file.convertString(_name);
        QString path = albumFolder+'/'+pageFolder;
        temp->folderPath = path;
        temp->listPhotos.pageName = temp->name;
        temp->listPhotos.pagePath = path;
        temp->next = NULL;
        //temp->listPages.albumName = folderName;
        //temp->debugPrint();

        if(file.createFolder(path) <0)
            return -1;
        /*if( createFolder(temp) < 0)
            return ERROR_UNKNOWN;*/
        //temp->debugPrint();
    }

    c_file testFile;
    testFile.saveList(this);

    return 0;
}

/*!
 * \brief c_listPage::create, creates a new page to add to the list. Suposed to be used for Travel or Other
 * \param _type - it should be either "Travel" or "Other"
 * \param _name - name of the page
 * \param _description - description of the page
 * \param _startDate - start date of the photos
 * \param _endDate - end date of the photos
 * \return 0 if no errors occur. -1 otherwise
 */
int c_listPage::create( QString _type, QString _name, QString _description, c_date _startDate, c_date _endDate){
    if(head == NULL){
        head = new(c_noPage);

        head->name = _name;
        head->type = _type;
        head->description=_description;
        head->startDate=_startDate;
        head->endDate=_endDate;

        head->next = NULL;

        c_file file;
        QString albumFolder = file.convertString(albumName);
        QString pageFolder = file.convertString(_name);
        QString path = albumFolder+'/'+pageFolder;
        head->folderPath = path;
        head->listPhotos.pageName = head->name;
        head->listPhotos.pagePath = path;
        if(file.createFolder(path) <0)
            return -1;
        //head->debugPrint();
    }
    else{
        if(checkIfExists(_name)){
            lastError = -1;
            return lastError;
        }
        c_noPage *temp = head;

        while(temp->next != NULL){
            temp = temp->next;
        }

        temp->next = new(c_noPage);
        temp =  temp->next;

        temp->name = _name;
        temp->type = _type;
        temp->description=_description;
        temp->startDate=_startDate;
        temp->endDate=_endDate;

        c_file file;
        QString albumFolder = file.convertString(albumName);
        QString pageFolder = file.convertString(_name);
        QString path = albumFolder+'/'+pageFolder;
        temp->folderPath = path;
        temp->listPhotos.pageName = temp->name;
        temp->listPhotos.pagePath = path;
        temp->next = NULL;
        //temp->listPages.albumName = folderName;
        //temp->debugPrint();

        if(file.createFolder(path) <0)
            return -1;
        /*if( createFolder(temp) < 0)
            return ERROR_UNKNOWN;*/
        //temp->debugPrint();
    }

    c_file testFile;
    testFile.saveList(this);

    return 0;
}


/*!
 * \brief c_listPage::remove
 * \param _name
 * \return 0 if no error occurs. ERROR_NOT_FOUND if the node to erase is not found
 */
int c_listPage::remove( QString _name){
    c_noPage *temp = head;
    c_noPage *previous = NULL;
    while(temp != NULL){

        if(temp->name == _name){

            if(previous != NULL)
                previous->next = temp->next;
            else
                head = temp->next;

            temp->listPhotos.removeAll();

            c_file file;
            QString albumFolder = file.convertString(albumName);
            QString pageFolder = file.convertString(temp->name);
            QString path = albumFolder+'/'+pageFolder;
            file.removeFolder(path);
            delete(temp);
            file.saveList(this);
            return 0;
        }

        previous = temp;
        temp = temp->next;
    }

    lastError = ERROR_NOT_FOUND;
    return lastError;
}

/*!
 * \brief c_listPage::removeAll, removes all pages from list
 * \return 0 if no errors occur. -1 otherwise
 */
int c_listPage::removeAll(){
    while(head != NULL){
        int error = remove(head->name);
        if(error < 0)
            return ERROR_UNKNOWN;
    }

    return 0;
}

/*!
 * \brief c_listPage::search, searches for page by name
 * \param _name - name of the page to be searched
 * \return the page node pointer or NULL if page not found
 */
c_noPage* c_listPage::search(QString _name){
    c_noPage *temp = head;
    while(temp != NULL){

        if(temp->name == _name){
            return temp;
        }
        temp = temp->next;
    }

    return NULL;
}

/*!
 * \brief c_listPage::checkIfExists, checks if a page with that name exists
 * \param _name - name of the page to be searched
 * \return 0 if page not found, 1 if found
 */
int c_listPage::checkIfExists(QString _name){
    if(_name == "Albums")
        return -1;
    c_noPage *temp = head;
    while(temp != NULL){

        if(temp->name == _name){
            return(-1);
        }
        temp = temp->next;
    }

    return(0);
}

/*!
 * \brief c_noPage::operator =, changes this values. The return really isn't anything.
 * \param _elementToAdd
 * \return Nothing
 */
c_noPage c_noPage::operator=(c_noPage _elementToAdd){

    /* Common to all */
     name=_elementToAdd.name;
     folderPath=_elementToAdd.folderPath;
     type=_elementToAdd.type;

    /* for party */
     date=_elementToAdd.date;
     partyType=_elementToAdd.partyType;
     description=_elementToAdd.description;

    /* for person or stuff */
    // QString description;

    /* for travel*/
    // QString description;
    startDate=_elementToAdd.startDate;
    endDate=_elementToAdd.endDate;

    /* for other */

    //*photoHead=_elementToAdd.photoHead;
    next=_elementToAdd.next;

    c_noPage temp;
    return temp;
}

/*!
 * \brief c_listPage::debugPrint, prints all pages in list
 * \return 0
 */
int c_listPage::debugPrint(){
    qDebug() << "====================";
    qDebug() << "Printing current page list from: " << albumName;
    c_noPage *temp = head;
    while(temp != NULL){
        temp->debugPrint();
        temp = temp->next;
    }
    qDebug() << "====================";

    return 0;
}

/*!
 * \brief c_listPage::add, adds a page node to the list
 * Copies the values of a element passed by parameter into a new node which is added to the list
 * \param _elementToAdd - element to be added.
 * \return 0 if page not found, 1 if found
 */
int c_listPage::add(c_noPage _elementToAdd){
    if(head == NULL){
        head = new(c_noPage);
        *head = _elementToAdd;
        /* Set pointers to NULL */
        head->next = NULL;
        //head->pagesHead = NULL;

        c_file file;
        QString albumFolder = file.convertString(albumName);
        QString pageFolder = file.convertString(_elementToAdd.name);
        QString path = albumFolder + '/' + pageFolder;
        head->folderPath = path;
        head->listPhotos.pageName = head->name;
        //albumName = folderName;
        if( file.createFolder(path) < 0)
            return -1;
        //head->debugPrint();
    }
    else{
        c_noPage *temp = head;

        while(temp->next != NULL){
            temp = temp->next;
        }

        temp->next = new(c_noPage);
        temp =  temp->next;

        *temp = _elementToAdd;
        /* Set pointers to NULL */
        temp->next = NULL;

        c_file file;
        QString albumFolder = file.convertString(albumName);
        QString pageFolder = file.convertString(_elementToAdd.name);
        QString path = albumFolder + '/' + pageFolder;
        temp->folderPath = path;
        temp->listPhotos.pageName = temp->name;
        //albumName = folderName;
        if( file.createFolder(path) < 0)
            return -1;
        //temp->debugPrint();
    }

    //c_file testFile;
    //testFile.saveList(head);

    return 0;
}

/*!
 * \brief c_listPage::openPage, open the page _name
 * \param _name
 * \return c_noPage address if it was found
 */

c_noPage* c_listPage::openPage(QString _name){
    c_noPage *temp = search(_name);
    if(temp->listPhotos.head == NULL)
        temp->listPhotos.loadList();
    temp->listPhotos.debugPrint();
    return temp;
}


/*!
 * \brief c_listPage::getPage, open the page _n
 * \param _n
 * \return c_noPage address if it was found
 */

c_noPage* c_listPage::getPage(int _n){

    c_noPage *temp = head;
    int k = 0;
    while(temp != NULL){

        if(k == _n){
            //if(_element != NULL)
            //    _element = temp;
            return temp;
        }
        temp = temp->next;
        k++;
    }
    return NULL;
}


/*!
 * \brief c_listPage::setElementIndex, set elementIndex with _n
 * \param _n
 */

void c_listPage::setElementIndex(int _n){
    elementIndex = _n;
}

/*!
 * \brief c_listPage::getPage
 * \return
 */

c_noPage* c_listPage::getPage(){

    c_noPage *temp = head;
    int k = 0;
    while(temp != NULL){

        if(k == elementIndex){
            elementIndex++;
            //if(_element != NULL)
            //    _element = temp;
            return temp;
        }
        temp = temp->next;
        k++;
    }
    if(elementIndex == 0)
        return NULL;

    elementIndex = 0;
    return getPage();
}

