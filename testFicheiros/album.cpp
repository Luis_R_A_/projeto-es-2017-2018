/*!
    \file
    \brief cpp file for module Album

    See the header album.h for more info

*/


#include "album.h"
#include <QDebug>
#include "file.h"

/*================================================
 * c_noAlbum methods
 ================================================*/
/*!
 * \brief c_noAlbum::c_noAlbum
 */
c_noAlbum::c_noAlbum(){
    next = NULL;
    //pagesHead = NULL;
}

/*!
 * \brief c_noAlbum::operator =, changes this values. The return really isn't anything.
 * \param _elementToAdd
 * \return Nothing
 */
c_noAlbum c_noAlbum::operator=(c_noAlbum _elementToAdd){
    c_noAlbum temp;
    description = _elementToAdd.description;
    endDate = _elementToAdd.endDate;
    folderName = _elementToAdd.folderName;
    name = _elementToAdd.name;
    //temp.next = NULL;
    //temp.pagesHead = NULL;
    pageTypes = _elementToAdd.pageTypes;
    startDate = _elementToAdd.startDate;
    timePeriod = _elementToAdd.timePeriod;

    return temp;
}



/*!
 * \brief c_noAlbum::debugPrint, this is just for debug purposes, it prints out the atributes
 * \return 0
 */
int c_noAlbum::debugPrint(){

    qDebug()  << "Name: " << name << " ; Description: " << description << " ; Folder: " << folderName << " ; Types: " << pageTypes
              << " ; StartDate: " << startDate.day << '/' << startDate.month << '/' << startDate.year
              << " ; EndDate: " << endDate.day << '/' << endDate.month << '/' << endDate.year
              << " ; timePeriod: " << timePeriod.day << '/' << timePeriod.month << '/' << timePeriod.year
              ;//<< '\n';
    return 0;
}

/*================================================
 * c_listAlbum methods
 ================================================*/

/*
 * Constructors
 */

/*!
 * \brief c_listAlbum::c_listAlbum, the default constructor
 *
 */
c_listAlbum::c_listAlbum(){

    head = NULL;
    lastError = 0;

}

/*
 * Private methods
 */
/*!
 * \brief c_listAlbum::checkIfExists, checks if a album with that name exists
 * \param _name - the name to check
 * \return 1 if the album exists, 0 if not
 */
int c_listAlbum::checkIfExists(QString _name){

    c_noAlbum *temp = head;
    while(temp != NULL){
        if(temp->name == _name){
            //if(_element != NULL)
            //    _element = temp;
            return 1;
        }
        temp = temp->next;
    }


    return 0;
}

//QString accent = "âêîôûŵŷäëïöüẅÿàèìòùẁỳáéíóúẃý";
//QString accentTo = "aeiouwyaeiouwyaeiouwyaeiouwyc";

///*!
// * \brief c_listAlbum::convertString, converts some of the special characters of the QString
// * It converts spaces to "_", eliminates the accents (keeps the letters) and removes the rest of the special characters.
// * \param _toConvert - QString to convert
// * \return the converted QString
// */
//QString c_listAlbum::convertString(QString _toConvert){
//    QString name = _toConvert;

//    name.replace(" ", "_");
//    int size = accent.size();
//    int nameSize = name.size();
//    for(int k = 0; k <nameSize; k++){
//        for(int i = 0; i < size; i++){
//            if(name.at(k) == accent.at(i)){
//                name.replace(k,1,accentTo.at(i));
//            }

//        }
//    }

//    name.remove(QRegExp("[-`~!@#$%^&*()—+=|:;<>«»,.?/{}\'\"\\\[\\\]\\\\]"));

//    return name;
//}

/*!
 * \brief c_listAlbum::createFolder, auxiliary method to create a folder with the correct name
 * \param _element, the album node to create a folder from
 * \return 0 if no error occurs
 */
int c_listAlbum::createFolder(c_noAlbum *_element){

    //QString albumName = convertString(_element->name);
    c_file temp;
    temp.createFolder(_element->folderName);

    return 0;
}

/*
 * Public methods
 */

/*!
 * \brief c_listAlbum::loadList, loads the album list from a file into memory
 * \return 0 if no error occurs. ERROR_UNKNOWN if any error occurs
 */
int c_listAlbum::loadList(){
    c_file file;
    int error = file.loadList(*this);
    if(error < 0)
        return ERROR_UNKNOWN;
    return 0;
}

/*!
 * \brief c_listAlbum::create,  creates a new album to add to the album list.
 * \param _name - name of the album
 * \param _description - description of the album
 * \param _pageTypes - types of pages supported (not implemented)
 * \return 0 if all is good. -2 if name already exists in database
 */
int c_listAlbum::create(QString _name, QString _description, QString _pageTypes){


    if(head == NULL){
        head = new(c_noAlbum);

        /* Set pointers to NULL */
        //head->next = NULL;
        //head->pagesHead = NULL;

        head->name = _name;
        head->description = _description;
        head->pageTypes = _pageTypes;
        c_file file;
        QString folderName = file.convertString(_name);
        head->folderName = folderName;

        head->next = NULL;
        head->listPages.albumName = folderName;
        //head->debugPrint();
        if( createFolder(head) < 0)
            return ERROR_UNKNOWN;
    }
    else{
        if(checkIfExists(_name)){
            lastError = ERROR_NAME_EXISTS;
            return lastError;
        }
        c_noAlbum *temp = head;

        while(temp->next != NULL){
            temp = temp->next;
        }

        temp->next = new(c_noAlbum);
        temp =  temp->next;
        /* Set pointers to NULL */
        //head->next = NULL;
        //head->pagesHead = NULL;

        temp->name = _name;
        temp->description = _description;
        temp->pageTypes = _pageTypes;

        c_file file;
        QString folderName = file.convertString(_name);
        temp->folderName = folderName;

        temp->next = NULL;
        temp->listPages.albumName = folderName;
        //temp->debugPrint();
        if( createFolder(temp) < 0)
            return ERROR_UNKNOWN;
    }



    c_file testFile;
    testFile.saveList(head);

    return 0;

}

/*!
 * \brief c_listAlbum::add, adds a album to the list.
 * It does not add it to the file, this is suposed to be used by the file class
 * \param _elementToAdd - node to add to the list
 * \return 0 if all is good
 */
int c_listAlbum::add(c_noAlbum _elementToAdd){
    if(head == NULL){
        head = new(c_noAlbum);
        *head = _elementToAdd;
        /* Set pointers to NULL */
        head->next = NULL;
        //head->pagesHead = NULL;

        c_file file;
        QString folderName = file.convertString(_elementToAdd.name);
        head->folderName = folderName;
        head->listPages.albumName = folderName;
        if( createFolder(head) < 0)
            return ERROR_UNKNOWN;
        //head->debugPrint();
    }
    else{
        c_noAlbum *temp = head;

        while(temp->next != NULL){
            temp = temp->next;
        }

        temp->next = new(c_noAlbum);
        temp =  temp->next;

        *temp = _elementToAdd;
        /* Set pointers to NULL */
        temp->next = NULL;
        //temp->pagesHead = NULL;
        c_file file;
        QString folderName = file.convertString(_elementToAdd.name);
        temp->folderName = folderName;
        temp->listPages.albumName = folderName;
        if( createFolder(temp) < 0)
            return ERROR_UNKNOWN;
        //temp->debugPrint();
    }

    //c_file testFile;
    //testFile.saveList(head);

    return 0;
}

/*!
 * \brief c_listAlbum::modifyName, changes the name of album
 * \param _name - name of the album to change name
 * \param _newName - new name for the album
 * \return 0 if no error occurs. ERROR_NOT_FOUND if the node to modify is not found
 */
int c_listAlbum::modifyName(QString _name, QString _newName){
    if(checkIfExists(_newName))
        return ERROR_NAME_EXISTS;

    c_noAlbum *temp = search(_name);

    c_file file;
    QString oldAlbumName = file.convertString(_name);
    QString newAlbumName = file.convertString(_newName);

    //c_file file;
    file.changeFolderName(oldAlbumName, newAlbumName);

    if(temp == NULL)
        return ERROR_NOT_FOUND;
    temp->name = _newName;
    temp->folderName = newAlbumName;
    temp->listPages.albumName = temp->name;

    file.saveList(head);
    return 0;
}

/*!
 * \brief c_listAlbum::modifyDescription - changes the description of a album
 * \param _name - name of the album to change name
 * \param _newDescription - new description for the album
 * \return 0 if no error occurs. ERROR_NOT_FOUND if the node to modify is not found
 */
int c_listAlbum::modifyDescription(QString _name, QString _newDescription){
    c_noAlbum *temp = search(_name);

    if(temp == NULL)
        return ERROR_NOT_FOUND;
    temp->description = _newDescription;

    return 0;

}

/*!
 * \brief c_listAlbum::remove
 * \param _name
 * \return 0 if no error occurs. ERROR_NOT_FOUND if the node to erase is not found
 */
int c_listAlbum::remove(QString _name){
    c_noAlbum *temp = head;
    c_noAlbum *previous = NULL;
    while(temp != NULL){

        if(temp->name == _name){

            if(previous != NULL)
                previous->next = temp->next;
            else
                head = temp->next; //should do, even if next is NULL, should solve itself out

            temp->listPages.removeAll();
            c_file testFile;
            testFile.removeFolder(testFile.convertString(temp->name));
            delete(temp);
            testFile.saveList(head);
            return 0;
        }

        previous = temp;
        temp = temp->next;
    }

    lastError = ERROR_NOT_FOUND;
    return lastError;
}

/*!
 * \brief c_listAlbum::removeAll, remove all albums from memory and file "database"
 * \return 0 if all good, ERROR_UNKNOWN otherwise
 */
int c_listAlbum::removeAll(){
    //c_noAlbum *temp = head;

    while(head != NULL){
        int error = remove(head->name);
        if(error < 0)
            return ERROR_UNKNOWN;
    }

    return 0;
}

/*!
 * \brief c_listAlbum::search, search by name
 * \param _name - name of the album to search for
 * \return c_noAlbum address if it was found. NULL otherwise
 */
c_noAlbum* c_listAlbum::search(QString _name){
    c_noAlbum *temp = head;
    while(temp != NULL){

        if(temp->name == _name){
            //if(_element != NULL)
            //    _element = temp;
            return temp;
        }
        temp = temp->next;
    }


    return NULL;
}

/*!
 * \brief c_listAlbum::getError, gives a string of the last error occured (not always implemented)
 * \return the string of the last error occured
 */
QString c_listAlbum::getError(){
    int error = (lastError * -1);
    if(error < 0)
        return NULL;
    if(error > N_ERRORS)
        return NULL;

    return errorString[error-1];
}

/*!
 * \brief c_listAlbum::getError, converts a negative value to the corresponding string error
 * \param _errorID - error ID, negative value
 * \return the string of the _errorID
 */
QString c_listAlbum::getError(int _errorID){

    int error = (_errorID * -1);
    if(error < 0)
        return NULL;
    if(error > N_ERRORS)
        return NULL;

    return errorString[error-1];
}

/*!
 * \brief c_listAlbum::debugPrint, prints all the albuns in the list
 * \return 0
 */
int c_listAlbum::debugPrint(){
    qDebug() << "====================";
    qDebug() << "Printing current album list";
    c_noAlbum *temp = head;
    while(temp != NULL){
        temp->debugPrint();
        temp = temp->next;     
    }
    qDebug() << "====================";

    return 0;
}

/*!
 * \brief c_listAlbum::openAlbum, opens a album
 * This will load the list of pages of a album
 * into memory and return the album node so that
 * the c_listPage methods can be used
 * \param _name - name of the album to open
 * \return pointer to the node of the opened album. NULL if not found
 */
c_noAlbum* c_listAlbum::openAlbum(QString _name){


    c_noAlbum *temp = search(_name);
    if(temp->listPages.head == NULL)
        temp->listPages.loadList();
    return temp;

}

/*!
 * \brief c_listAlbum::getAlbum, search the node
 * \return c_noPeople address if it was found. NULL otherwise
 */
c_noAlbum* c_listAlbum::getAlbum(int _n){

    c_noAlbum *temp = head;
    int k = 0;
    while(temp != NULL){
         //qDebug() << k;
        if(k == _n){
            //if(_element != NULL)
            //    _element = temp;
            return temp;
        }
        temp = temp->next;
        k++;
    }
    return NULL;
}

/*!
 * \brief c_listAlbum::setElementIndex, set elementIndex with _n
 * \param _n
 * \return -
 */
void c_listAlbum::setElementIndex(int _n){
    elementIndex = _n;
}

/*!
 * \brief c_listAlbum::getAlbum
 * \return
 */
c_noAlbum* c_listAlbum::getAlbum(){

    c_noAlbum *temp = head;
    int k = 0;
    while(temp != NULL){

        if(k == elementIndex){
            elementIndex++;
            //if(_element != NULL)
            //    _element = temp;
            return temp;
        }
        temp = temp->next;
        k++;
    }
    if(elementIndex == 0)
        return NULL;


    elementIndex = 0;
    return getAlbum();
}
