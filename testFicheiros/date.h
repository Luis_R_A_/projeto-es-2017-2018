#ifndef DATE_H
#define DATE_H

#include <QString>
/*
    \defgroup Date_Package Date
    @{
    \brief Class for date formats
*/
/*!
    \ingroup Gestao_Package Gestão
    @{
*/
class c_date{

  private:
    const QString monthsString[12] = {"January", "February", "March", "April", "May", "June", "July",
                                        "August", "September", "October", "November", "December"};
  public:
    int day;
    int month;
    int year;


    c_date operator+(c_date _date2);
    c_date operator-(c_date _date2Subtract);
    c_date operator=(c_date _date2);

    c_date();
};

/*!
  @}
 */
#endif // DATE_H
