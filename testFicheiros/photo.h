#ifndef PHOTO_H
#define PHOTO_H

/*
    \defgroup Photo_Package Photo
    @{
    \brief Class for managing photo lists
*/
/*!
    \ingroup Gestao_Package
	\addtogroup Gestao_Package
    @{

*/
#include "date.h"
#include <QString>
#include "people.h"
typedef struct s_resolution{
    int x;
    int y;
}s_resolution;


class c_noPhoto{

public:
    s_resolution resolution;
    c_date date;
    QString filePath;
    QString name;

    c_listPeople listPeople;
    c_noPhoto *next;

    c_noPhoto();
    //~c_noPhoto();

    c_noPhoto operator=(c_noPhoto _elementToAdd);

    int debugPrint();
    int add(c_noPeople _elementToAdd);
    int remove(QString _name);
};

class c_listPhoto{
private:

    int elementIndex;
public:
    c_noPhoto* head;
    QString pageName;
    QString pagePath;

    c_listPhoto();
    //~c_listPhoto();

    int loadList();
    int remove(QString _name);
    int create(s_resolution _resolution, c_date _date, QString _name);
    int removeAll();
    int checkIfExists(QString name);

    int add(c_noPhoto _elementToAdd);
    //c_listPhoto* search(s_resolution _resolution);
    //c_listPhoto* search(c_date _date);

    int debugPrint();

    c_noPhoto* getPhoto(int _n);
    void setElementIndex(int _n);
    c_noPhoto* getPhoto();

};

/*!
 * @}
 */
#endif // PHOTO_H
