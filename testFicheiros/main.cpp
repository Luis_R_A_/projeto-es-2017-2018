#include <QCoreApplication>

#include <iostream>

#include "album.h"
#include "date.h"
#include "file.h"
#include "page.h"
#include "photo.h"
#include "people.h"

#include <QDebug>

c_listAlbum albumList;
c_listPeople peopleList;
//c_file testFile;

void test0();
void test1CreateAlbuns();
void test2LoadAlbuns();
void test3RemoveAlbuns();
void test4ChangeOneAlbum();
void test5RemoveAll();
void test5lists();

void test2LoadListOfPeople();
void test5RemoveAllListsOfPeople();
void test1CreateListOfPeople();
void test3RemoveListOfPeople();
void test4ChangeOneListOfPeople();

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    //run this once to create first file for tests.
    //test1CreateAlbuns();
    //return 0;


    /*test2LoadAlbuns(); //this is what should happen when the program starts

    test5RemoveAll();

    test1CreateAlbuns();

    test3RemoveAlbuns();

    test4ChangeOneAlbum();*/

    //test5lists();


    //test2LoadListOfPeople();


    peopleList.debugPrint();

    test1CreateListOfPeople();

    test3RemoveListOfPeople();

    test4ChangeOneListOfPeople();

    test5RemoveAllListsOfPeople();



    return a.exec();
}

void test0(){
    c_file testFile;
    c_noAlbum *head;
    head = new(c_noAlbum);
    head->name = "Album1";
    head->description = "Small test for saving files";
    head->folderName = "folder";
    head->pageTypes = "testType";
    head->startDate.day = 1;
    head->startDate.month = 2;
    head->startDate.year = 2018;
    head->endDate.day = 23;
    head->endDate.month = 11;
    head->endDate.year = 2018;
    head->timePeriod.day = 0;
    head->timePeriod.month = 0;
    head->timePeriod.year = 0;
    head->next = NULL;
    testFile.saveList(head);

    c_noAlbum *test;
    test = new(c_noAlbum);
    testFile.loadList(test);

    qDebug() << "Name: " << test->name << " ; Description: " << test->description << " ; Folder: " << test->folderName << " ; Types: " << test->pageTypes
             << " ; StartDate: " << test->startDate.day << '/' << test->startDate.month << '/' << test->startDate.year
             << " ; EndDate: " << test->endDate.day << '/' << test->endDate.month << '/' << test->endDate.year
             << " ; timePeriod: " << test->timePeriod.day << '/' << test->timePeriod.month << '/' << test->timePeriod.year
             << '\n';
}
void test1CreateAlbuns(){

    albumList.create("Album1 Luís àú123", "Test Album create", "I dunno");

    if(albumList.create("Album1 Luís àú123", "Test Album create", "I dunno") < 0)
        qDebug() << "Error creating: " << albumList.getError();
    if(albumList.create("Album2", "Test Album create 2", "I still dunno") <0)
        qDebug() << "Error creating: " << albumList.getError();
    if(albumList.create("Album3", "Test Album create 3", "I still dunno 2") <0)
        qDebug() << "Error creating: " << albumList.getError();
    if(albumList.create("Album4", "Test Album create 4", "I still dunno 3") <0)
        qDebug() << "Error creating: " << albumList.getError();
    if(albumList.create("Album5", "Test Album create 5", "I still dunno 4") <0)
        qDebug() << "Error creating: " << albumList.getError();
    if(albumList.create("Album1", "Test Album create 5", "I still dunno 4") <0)
        qDebug() << "Error creating: " << albumList.getError();
    if(albumList.create("Album Luís!!", "Test Album create", "I dunno") < 0)
        qDebug() << "Error creating: " << albumList.getError();
    albumList.debugPrint();
}
void test2LoadAlbuns(){
    if(albumList.loadList() < 0)
        qDebug() << "Error loading: " << albumList.getError();
    albumList.debugPrint();
}

void test3RemoveAlbuns(){
    QString temp = "Album3";
    if(albumList.remove(temp) < 0){
        qDebug() << "Error removing: " << albumList.getError();
    }
    albumList.debugPrint();
}

void test4ChangeOneAlbum(){
    if(albumList.modifyName("Album2", "Album#!?#XPTO") < 0)
        qDebug() << "Error renaming: " << albumList.getError();
    albumList.debugPrint();
}

void test5RemoveAll(){
    albumList.removeAll();
    albumList.debugPrint();
}

void test5lists(){
    c_noAlbum *openedAlbum = albumList.openAlbum("Album#!?#XPTO");
    if(openedAlbum == NULL){
        qDebug() << "Error, album to open not found\n";
        return;
    }
    openedAlbum->listPages.debugPrint();
    c_date date, date2;
    //openedAlbum->listPages.loadList();
    if( openedAlbum->listPages.create("Party", "5 anos Luis 2018", "PAAAARTY", date, "Aniversário") < 0)
        qDebug() << "Error, adding page\n";
    if( openedAlbum->listPages.create("Person or stuff", "1 anos Luis 2018", "PAAAARTY") < 0)
        qDebug() << "Error, adding page\n";
    if( openedAlbum->listPages.create("Travel", "2 anos Luis 2018", "PAAAARTY", date, date2) < 0)
        qDebug() << "Error, adding page\n";
    if( openedAlbum->listPages.create("Other", "3 anos Luis 2018", "PAAAARTY", date, date2) < 0)
        qDebug() << "Error, adding page\n";
    openedAlbum->listPages.debugPrint();


    c_noPage *openedPage = openedAlbum->listPages.openPage("1 anos Luis 2018");
    openedPage->listPhotos.debugPrint();
    s_resolution r = {0, 0};

    openedPage->listPhotos.create(r, date, "photo1");
    openedPage->listPhotos.create(r, date, "photo2");
    openedPage->listPhotos.create(r, date, "photo3");

    //openedPage->listPhotos.removeAll();

    openedPage->listPhotos.debugPrint();
    //openedAlbum->listPages.removeAll();
    openedAlbum->listPages.debugPrint();

    //albumList.removeAll();
}

void test1CreateListOfPeople(){
    c_date birthday;
    birthday.day=1;
    birthday.month=1;
    birthday.year=1;

    if(peopleList.create("People", birthday, "Masculino", "Pai") < 0)
        qDebug() << "Error creating: " << peopleList.getError();
    if(peopleList.create("People1", birthday, "Feminino", "Mãe") < 0)
        qDebug() << "Error creating: " << peopleList.getError();
    if(peopleList.create("People2", birthday, "tosta mista", "lanche") < 0)
        qDebug() << "Error creating: " << peopleList.getError();
    if(peopleList.create("People1", birthday, "Feminino", "Mãe") < 0)
        qDebug() << "Error creating: " << peopleList.getError();

    peopleList.debugPrint();
}

void test2LoadListOfPeople(){
    if(peopleList.loadList() < 0)
        qDebug() << "Error loading: " << peopleList.getError();
    peopleList.debugPrint();
}

void test3RemoveListOfPeople(){
    QString temp = "People";
    if(peopleList.remove(temp) < 0){
        qDebug() << "Error removing: " << peopleList.getError();
    }
    peopleList.debugPrint();
}

void test4ChangeOneListOfPeople(){
    if(peopleList.modifyName("People1", "People3") < 0)
        qDebug() << "Error renaming: " << peopleList.getError();
    peopleList.debugPrint();
}

void test5RemoveAllListsOfPeople(){
    peopleList.removeall();
    peopleList.debugPrint();
}
