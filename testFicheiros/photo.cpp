/*!
    \file
    \brief cpp file for module Photo

    See the header photo.h for more info

*/

#include "photo.h"
#include "file.h"

#include <QDebug>

/*!
 * \brief c_noPhoto::c_noPhoto
 */
c_noPhoto::c_noPhoto(){
    resolution.x = 0;
    resolution.y = 0;
    date.day = 0;
    date.month = 0;
    date.year = 0;
    name="";

    filePath = "";

    next = NULL;
}

/*!
 * \brief c_noPhoto::operator =, changes this values. The return really isn't anything.
 * \param _elementToAdd
 * \return Nothing
 */
c_noPhoto c_noPhoto::operator=(c_noPhoto _elementToAdd){

    /* Common to all */
     name=_elementToAdd.name;
     filePath=_elementToAdd.filePath;
     date=_elementToAdd.date;
     resolution.x = _elementToAdd.resolution.x;
     resolution.y = _elementToAdd.resolution.x;

    next=_elementToAdd.next;

    c_noPhoto temp;
    return temp;
}

/*!
 * \brief c_noPhoto::add, add people to the people list.
 * \param _elementToAdd, node to add to the list
 * \return 0 if all is good
 */
int c_noPhoto::add(c_noPeople _elementToAdd){

    //listPeople.create(_elementToAdd.name, _elementToAdd.birthday, _elementToAdd.gender, _elementToAdd.relation);
    return 0;
}

/*!
 * \brief c_noPhoto::remove, remove people from the people list.
 * \param _name, name to remove the list
 * \return 0 if all is good
 */
int c_noPhoto::remove(QString _name){

    //listPeople.remove(_name);
    return 0;
}

/*!
 * \brief c_listPhoto::loadList loads the photo list from a file into memory
 * \return 0 if no error occurs. -1 if any error occurs
 */
int c_listPhoto::loadList(){
    c_file file;
    int error = file.loadList(*this);
    if(error < 0)
        return -1;
    return 0;
}

/*!
 * \brief c_listPhoto::c_listPhoto
 */
c_listPhoto::c_listPhoto(){

    head = NULL;
}

/*!
 * \brief c_listPhoto::create,creates a new photo to add to the photo list.
 * \param _resolution - resolution of the photo
 * \param _date - date of the photo
 * \param _name - name of the photo
 * \return 0 if all is good. -2 if name already exists in database
 */
int c_listPhoto::create(s_resolution _resolution, c_date _date, QString _name){

    if(checkIfExists(_name))
        return -1;
    if(head == NULL){
        head = new(c_noPhoto);

        /* Set pointers to NULL */
        //head->next = NULL;
        //head->pagesHead = NULL;

        c_file file;
        head->resolution.x = _resolution.x;
        head->resolution.y = _resolution.y;
        head->date = _date;
        head->filePath = pagePath + '/' + _name;//file.convertString((_name));
        head->name=_name;

        //QString pageFolder = pageName;
        //QString fileName = file.convertString(_name);
        //QString path = pageFolder+'/'+fileName;
        //head->filePath = path;
       /* if(file.createFolder(path) <0)
            return -1;*/
    }
    else{
        /*if(checkIfExists(_name)){
            lastError = ERROR_NAME_EXISTS;
            return lastError;
        }*/
        c_noPhoto *temp = head;

        while(temp->next != NULL){
            temp = temp->next;
        }

        temp->next = new(c_noPhoto);
        temp =  temp->next;

        temp->resolution.x = _resolution.x;
        temp->resolution.y = _resolution.y;
        temp->date = _date;


         c_file file;
        //c_file testFile;
        //QString folderName = convertString(_filePath);
        temp->filePath = pagePath + '/' + _name;//file.convertString((_name));
        temp->name=_name;

        //c_file file;
        //QString pageFolder = pageName;
        //QString fileName = file.convertString(_name);
        //QString path = pageFolder+'/'+fileName;
        //temp->filePath = path;
        /*if(file.createFolder(path) <0)
            return -1;*/
    }



    c_file testFile;
    testFile.saveList(this);

    return 0;

}

/*!
 * \brief c_listPhoto::remove
 * \param _name
 * \return 0 if no error occurs. ERROR_NOT_FOUND if the node to erase is not found
 */
int c_listPhoto::remove(QString _name){
    c_noPhoto *temp = head;
    c_noPhoto *previous = NULL;
    while(temp != NULL){

        if(temp->name == _name){

            if(previous != NULL)
                previous->next = temp->next;
            else
                head = temp->next;
            /*if(temp->next != NULL){

            }*/
            c_file file;
//            QString albumFolder = file.convertString(_name);
//            QString pageFolder = file.convertString(temp->name);
//            QString path = albumFolder+'/'+pageFolder;
//            file.removeFolder(path);
            delete(temp);
            file.saveList(this);
            return 0;
        }

        previous = temp;
        temp = temp->next;
    }

    //lastError = ERROR_NOT_FOUND;
    //return lastError;
    return -1;
}

/*!
 * \brief c_listPhoto::removeAll, remove all photos from memory
 * \return 0 if all good, ERROR_UNKNOWN otherwise
 */
int c_listPhoto::removeAll(){
    while(head != NULL){
        int error = remove(head->name);
        if(error < 0)
            return ERROR_UNKNOWN;
    }

    return 0;
}

/*!
 * \brief c_listPhoto::checkIfExists, checks if a photo with that name exists
 * \param _name - the name to check
 * \return 1 if the photo exists, 0 if not
 */
int c_listPhoto::checkIfExists(QString _name){
    c_noPhoto *temp = head;
    while(temp != NULL){
        if(temp->name == _name){
            //if(_element != NULL)
            //    _element = temp;
            return 1;
        }
        temp = temp->next;
    }


    return 0;
}

/*!
 * \brief c_listPhoto::add, adds a photo to the list.
 * \param _elementToAdd - node to add to the list
 * \return 0 if all is good
 */
int c_listPhoto::add(c_noPhoto _elementToAdd){
    if(head == NULL){
        head = new(c_noPhoto);
        *head = _elementToAdd;
        /* Set pointers to NULL */
        head->next = NULL;
        //head->pagesHead = NULL;

        c_file file;
        QString pageFolder = pageName;
        QString fileName = _elementToAdd.name;//file.convertString(_elementToAdd.name);
        //QString path = pageFolder+'/'+fileName;
        head->filePath = _elementToAdd.filePath;
        //albumName = folderName;
        /*if( file.createFolder(path) < 0)
            return -1;*/
        //head->debugPrint();
    }
    else{
        c_noPhoto *temp = head;

        while(temp->next != NULL){
            temp = temp->next;
        }

        temp->next = new(c_noPhoto);
        temp =  temp->next;

        *temp = _elementToAdd;
        /* Set pointers to NULL */
        temp->next = NULL;

        c_file file;
        QString pageFolder = pageName;
        QString fileName = _elementToAdd.name;//file.convertString(_elementToAdd.name);
        //QString path = pageFolder+'/'+fileName;
        temp->filePath = _elementToAdd.filePath;
        //albumName = folderName;
        /*if( file.createFolder(path) < 0)
            return -1;*/
        //temp->debugPrint();
    }

    //c_file testFile;
    //testFile.saveList(head);

    return 0;
}

/*!
 * \brief c_noPhoto::debugPrint, it prints the atributes
 * \return 0
 */
int c_noPhoto::debugPrint(){
    qDebug()  << "Resolution: x " << resolution.x << "; y " << resolution.y << " ; dia: " << date.day
              << " ; mes: " << date.month << " ; ano: " << date.year
              << " ; Filepath: " << filePath << " ; name: " << name ;// << '\n';
    return 0;
}

/*!
 * \brief c_listPhoto::debugPrint, prints all the photos in the list
 * \return 0
 */
int c_listPhoto::debugPrint(){
    qDebug() << "====================";
    qDebug() << "Printing current photo list from: " << pageName;
    c_noPhoto *temp = head;
    while(temp != NULL){
        temp->debugPrint();
        temp = temp->next;
    }
    qDebug() << "====================";

    return 0;
}

/*!
 * \brief c_listPhoto::getPhoto, search the node
 * \param _n
 * \return c_noPeople address if it was found. NULL otherwise
 */
c_noPhoto* c_listPhoto::getPhoto(int _n){

    c_noPhoto *temp = head;
    int k = 0;
    while(temp != NULL){

        if(k == _n){
            //if(_element != NULL)
            //    _element = temp;
            return temp;
        }
        temp = temp->next;
        k++;
    }
    return NULL;
}

/*!
 * \brief c_listPhoto::setElementIndex, set elementIndex with _n
 * \param _n
 * \return -
 */
void c_listPhoto::setElementIndex(int _n){
    elementIndex = _n;
}

/*!
 * \brief c_listPhoto::getPhoto, search the node
 * \return c_noPhoto* address if it was found. NULL otherwise
 */
c_noPhoto* c_listPhoto::getPhoto(){

    c_noPhoto *temp = head;
    int k = 0;
    while(temp != NULL){

        if(k == elementIndex){
            elementIndex++;
            //if(_element != NULL)
            //    _element = temp;
            return temp;
        }
        temp = temp->next;
        k++;
    }
    if(elementIndex == 0)
        return NULL;

    elementIndex = 0;
    return getPhoto();
}
/*c_listPhoto* c_listPhoto::search(s_resolution _resolution){
    c_noPhoto *temp = head;
    while(temp != NULL){
        if(temp->s_resolution == _resolution){
            //if(_element != NULL)
            //    _element = temp;
            return temp;
        }
        temp = temp->next;
    }


    return NULL;
}

c_listPhoto* c_listPhoto::search(c_date _date){
    c_noPhoto *temp = head;
    while(temp != NULL){

        if(temp->date == _date){
            //if(_element != NULL)
            //    _element = temp;
            return temp;
        }
        temp = temp->next;
    }


    return NULL;
}
*/

