#ifndef FILES_H
#define FILES_H

#include "album.h"
#include "page.h"
#include "photo.h"

#include <string>

#include <QDir> //to create folders
#include <QString> //to create folders



/*
    \defgroup File_Package File
    @{
    \brief Class for managing files
*/
/*!
    \defgroup Ficheiros_Package Ficheiros
    @{
    \brief Module that allows to access files. See more in \ref Ficheiros_section
*/
#define DEFAULT_ROOT "default_root"
#define DEFAULT_ALBUM_FILE "Albums"
class c_file{

    private:

    QString rootPath;
    QString albumFiles;
    //int saveBytes(QString _path, void *_data, int _size);
    //int readBytes(QString _path, void *_data, int _maxSize);


    public:

    c_file();
    ~c_file();

    int createFolder(QString _path);
    int changeFolderName(QString _path, QString _newPath);
    int removeFolder(QString _path);
    int createFile(QString _path);

    /* For albums */
    int loadList(c_listAlbum &_album);
    int loadList(c_noAlbum *_head);
    int saveList(c_noAlbum *_head);


    /* For pages */
    int loadList(c_listPage &_list);
    int saveList(c_listPage *_list);

    /* For photos */
    int loadList(c_listPhoto &_list);
    int saveList(c_listPhoto *_list);


    QString convertString(QString _toConvert);

    int loadList(c_listPeople &_list);
    int saveList(c_listPeople *_list);




};

/*!
  @}
*/
#endif // FILES_H


