#ifndef PEOPLE_H
#define PEOPLE_H


#include <QString>
#include "date.h"
#include <QDebug>
//#include "people.h"

/*
    \defgroup People_Package People
    @{
    \brief Class for managing people lists
*/
/*!
    \ingroup Gestao_Package
	\addtogroup Gestao_Package
    @{
*/
class c_noPeople{
public:
    QString name;
    c_date birthday;
    QString gender;
    QString relation;

    c_noPeople *next;
    int debugPrint();

};


#define ERROR_UNKNOWN -1
#define ERROR_NAME_EXISTS -2
#define ERROR_NOT_FOUND -3
#define N_ERRORS 3
class c_listPeople{

private:
    int lastError;

    int checkIfExists(QString _name);

public:
    c_noPeople *head;
    QString name;
    int create(QString _name, c_date _birthday, QString _gender, QString _relation);
    c_noPeople* search(QString _name);
    int remove(QString _name);
    int loadList();
    int modifyName(QString _name, QString _newName);
    int removeall();
    QString getError();
    QString errorString[N_ERRORS] = {"UNKNOWN", "NAME ALREADY EXISTS", "NOT_FOUND"};
    int add(c_noPeople _elementToAdd);

    int debugPrint();

};
/*!
 * @}
 */
#endif // PEOPLE_H
