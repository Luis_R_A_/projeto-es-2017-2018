#ifndef ALBUM_H
#define ALBUM_H


#include "date.h"
#include "page.h"

#include <QString>
/*!
    \defgroup Gestao_Package Gestão
    @{
    \brief Module that manages all the lists of albums, pages, photos and people. See more in \ref Gestao_section
*/
class c_noAlbum{


public:

    /* album characteristics */
    QString name;
    QString description;
    QString pageTypes; //was it really necessary? Is string the best option?
    QString folderName;

    c_date startDate;
    c_date endDate;
    c_date timePeriod;

    /* linked list pointers */
    c_listPage listPages;
    c_noAlbum *next;

    c_noAlbum();

   int debugPrint();
   c_noAlbum operator=(c_noAlbum _elementToAdd);
};


#define ERROR_UNKNOWN -1
#define ERROR_NAME_EXISTS -2
#define ERROR_NOT_FOUND -3
#define N_ERRORS 3
class c_listAlbum{
private:
    c_noAlbum *head;
    int elementIndex = 0;

    int lastError;
    QString errorString[N_ERRORS] = {"UNKNOWN", "NAME ALREADY EXISTS", "NOT_FOUND"};
    int updateStartDate();
    int updateEndDate();
    int checkIfExists(QString _name);
    QString convertString(QString _toConvert);
    int createFolder(c_noAlbum *_element);
public:


    c_listAlbum();
    //~c_listAlbum();

    int loadList();
    int create(QString _name, QString _description, QString _pageTypes);
    //int create(QString _name, QString _description, QString _pageTypes, QString folderName, c_date startDate, c_date endDate, c_date timePeriod);
    int add(c_noAlbum _elementToAdd);
    int modifyName(QString _name, QString _newName);
    int modifyDescription(QString _name, QString _newDescription);
    int remove(QString _name);
    int removeAll();

    c_noAlbum* search(QString _name);

    QString getError();
    QString getError(int _errorID);
    int debugPrint();

    c_noAlbum* openAlbum(QString _name);

    c_noAlbum* getAlbum(int _n);
    void setElementIndex(int _n);
    c_noAlbum* getAlbum();

};

/*!
 * @}
 */
#endif // ALBUM_H
