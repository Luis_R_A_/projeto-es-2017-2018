/*!
    \file
    \brief cpp file for module People

    See the header people.h for more info

*/

#include <QString>
#include "date.h"
#include <QDebug>
#include "people.h"
#include "file.h"

/*!
 * \brief c_noPeople::debugPrint(), prints this node atributes
 * \return 0
 */
int c_noPeople::debugPrint(){
    qDebug()  << "Name: " << name << " ; birthday: " << birthday.day << '/' << birthday.month << '/' << birthday.year << " ; gender: " << gender << " ; relation: " << relation;
    return 0;
}

/*!
 * \brief c_listPeople::checkIfExists, checks if people with that name exist
 * \param _name - the name to check
 * \return 1 if people exist, 0 if not
 */
int c_listPeople::checkIfExists(QString _name){
    c_noPeople *temp = head;
    while(temp != NULL){
        if(temp->name == _name){
            return 1;
        }
        temp = temp->next;
    }


    return 0;
}

/*!
 * \brief c_listPeople::create, creates a new node to add to the people list.
 * \param _name - name
 * \param _birthday - birthday
 * \param _gender - gender
 * \param _relation - relation
 * \return 0 if no errors occur. -1 otherwise
 */
int c_listPeople::create(QString _name, c_date _birthday, QString _gender, QString _relation){
    if(head == NULL){
        head = new(c_noPeople);

        head->name = _name;
        head->birthday = _birthday;
        head->gender=_gender;
        head->relation=_relation;

        head->next = NULL;
    }
    else{
        if(checkIfExists(_name)){
            lastError = -1;
            return lastError;
        }
        c_noPeople *temp = head;

        while(temp->next != NULL){
            temp = temp->next;
        }

        temp->next = new(c_noPeople);
        temp =  temp->next;

        temp->name = _name;
        temp->birthday = _birthday;
        temp->gender=_gender;
        temp->relation=_relation;

        temp->next = NULL;
    }

    return 0;
}

/*!
 * \brief c_listPeople::search, search by name
 * \param _name - name of people to search for
 * \return c_noPeople address if it was found. NULL otherwise
 */
c_noPeople* c_listPeople::search(QString _name){
    c_noPeople *temp = head;
    while(temp != NULL){

        if(temp->name == _name){
            return temp;
        }
        temp = temp->next;
    }

    return NULL;
}

/*!
 * \brief c_listPeople::remove
 * \param _name
 * \return 0 if no error occurs. ERROR_NOT_FOUND if the node to erase is not found
 */
int c_listPeople::remove(QString _name){
    c_noPeople *temp = head;
    c_noPeople *previous = NULL;
    while(temp != NULL){

        if(temp->name == _name){

            if(previous != NULL)
                previous->next = temp->next;
            else
                head = temp->next;

            delete(temp);
            return 0;
        }

        previous = temp;
        temp = temp->next;
    }

    lastError = ERROR_NOT_FOUND;
    return lastError;
}

/*!
 * \brief c_listPeople::loadList, loads the people list from a file into memory
 * \return 0 if no error occurs. ERROR_UNKNOWN if any error occurs
 */
int c_listPeople::loadList(){
    c_file file;
    //int error = file.loadList(*this);
    //if(error < 0)
    //    return ERROR_UNKNOWN;
    return 0;
}

/*!
 * \brief c_listPeople::modifyName, changes the name of a node in the list
 * \param _name - name of the album to change name
 * \param _newName - new name for the album
 * \return 0 if no error occurs. ERROR_NOT_FOUND if the node to modify is not found
 */
int c_listPeople::modifyName(QString _name, QString _newName){
    if(checkIfExists(_newName))
        return ERROR_NAME_EXISTS;

    c_noPeople *temp = search(_name);

    c_file file;
    QString oldPeopleName = file.convertString(_name);
    QString newPeopleName = file.convertString(_newName);

    //c_file file;
    file.changeFolderName(oldPeopleName, newPeopleName);

    if(temp == NULL)
        return ERROR_NOT_FOUND;
    temp->name = _newName;

    return 0;
}

/*!
 * \brief c_listPeople::removeall, remove the list of people from memory and file "database"
 * \return 0 if all good, ERROR_UNKNOWN otherwise
 */
int c_listPeople::removeall(){
    while(head != NULL){
        int error = remove(head->name);
        if(error < 0)
            return ERROR_UNKNOWN;
    }

    return 0;
}

/*!
 * \brief c_listPeople::getErro, gives a string of the last error occured (not always implemented)
 * \return the string of the last error occured
 */
QString c_listPeople::getError(){
    int error = (lastError * -1);
    if(error < 0)
        return NULL;
    if(error > N_ERRORS)
        return NULL;

    return errorString[error-1];
}

/*!
 * \brief c_listPeople::debugPrint, prints all the people in the list
 * \return 0
 */
int c_listPeople::debugPrint(){
    qDebug() << "====================";
    qDebug() << "Printing current people list";
    c_noPeople *temp = head;
    while(temp != NULL){
        temp->debugPrint();
        temp=temp->next;
    }
    qDebug() << "====================";
    return 0;
}

/*!
 * \brief c_listPeople::add, adds a node to the list.
 * \param _elementToAdd - node to add to the list
 * \return 0 if all is good
 */
int c_listPeople::add(c_noPeople _elementToAdd){
    if(head == NULL){
        head = new(c_noPeople);
        *head = _elementToAdd;
        /* Set pointers to NULL */
        head->next = NULL;
    }
    else{
        c_noPeople *temp = head;

        while(temp->next != NULL){
            temp = temp->next;
        }

        temp->next = new(c_noPeople);
        temp =  temp->next;

        *temp = _elementToAdd;
        /* Set pointers to NULL */
        temp->next = NULL;
    }

    //c_file testFile;
    //testFile.saveList(head);

    return 0;
}
