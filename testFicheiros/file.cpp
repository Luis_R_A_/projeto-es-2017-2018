/*!
    \file
    \brief cpp file for class c_file

    See the header file.h for more info

*/




#include "file.h"
#include <fstream>
#include <QFile> //to open files
#include <QTextStream> //to write text into files

#include <QDebug>

QDir dir;

/*!
 * \brief c_file::createFolder, creates a folder in the path specified
 * This will create the intermediate folders needed if they don't exist
 * \param _path - the path of the folder to be created
 * \return 0 if success, -1 otherwise
 */
int c_file::createFolder(QString _path){
    QString folderPath = _path;//rootPath+'/'+_path;
    //std::fstream  outfile;
    if(dir.exists(folderPath))
        return 1;
    if(!dir.mkpath(folderPath))
        return -1;
    return 0;
}

/*!
 * \brief c_file::changeFolderName, changes a folder path if it exists
 * Note that this takes a path
 * \param _path - path of of the dir to change
 * \param _newPath - new path of the folder
 * \return 0 if success, -1 otherwise
 */
int c_file::changeFolderName(QString _path, QString _newPath){
    //std::fstream  outfile;
    if(dir.exists(_path)){
        dir.rename(_path, _newPath);
        return 0;
    }
    return -1;
}

/*!
 * \brief c_file::removeFolder, removes a folder
 * \param _path - path of the folder to be removed
 * \return 0 if success, -1 otherwise
 */
int c_file::removeFolder(QString _path){
    //std::fstream  outfile;
    if(dir.exists(_path)){
        dir.rmdir(_path);
        return 0;
    }
    return -1;
}


///*!
//    Private function to save generic bytes. I don't remember why I needed this.

//    \param *_path QString path of the file to be read
//    \param *_data is the data to save
//    \param _size is the max number of bytes to save


//    \returns 0 if no error occurs, -1 if a error occurs

//*/
//int c_file::saveBytes(QString _path, void *_data, int _size){

//    return 0;
//}

///*!
//    Private function to read generic bytes. I don't remember why I needed this.

//    \param *_path QString path of the file to be read
//    \param *_data where to save the data in memory
//    \param _maxSize is the max number of bytes to read


//    \returns 0 if no error occurs, -1 if a error occurs

//*/
//int c_file::readBytes(QString _path, void *_data, int _maxSize){

//    return 0;
//}


/*!
 * \brief c_file::createFile, Creates a file
 * \param _path QString path of the file to be created
 * \return 0 if no error occurs, -1 if a error occurs
 */
int c_file::createFile(QString _path){

    QString filePath = rootPath+'/'+_path+".txt";
    //std::fstream  outfile;
    QFile file(filePath);

    if(!file.open(QIODevice::ReadWrite | QIODevice::Text  |QIODevice::Append))
        return -1;

    QTextStream out(&file);
    //out.setCodec("UTF-8");
    //out << "The magic number is: " << 49 << "\n";

    return 0;
}

c_file::c_file(){
    rootPath = DEFAULT_ROOT;
    albumFiles = DEFAULT_ALBUM_FILE;

    dir.mkdir(rootPath);
}

c_file::~c_file(){

}

/*!
 * \brief c_file::loadList, loads a list from a file to a album list
 * \param _album - album to be loaded
 * \return 0 if success, -1 otherwise
 */
int c_file::loadList(c_listAlbum &_album){
    QString filePath = rootPath+'/'+albumFiles+".txt";
    //std::fstream  outfile;
    QFile file(filePath);
    if(!file.open(QIODevice::ReadWrite | QIODevice::Text | QIODevice::Text |QIODevice::Append))
        return -1;

    QTextStream in(&file);
    //in.setCodec("UTF-16");

    file.seek(0);
    c_noAlbum *temp = new(c_noAlbum);
    while (!in.atEnd()) {
        QString sTemp = in.read(5);
        int iTemp = sTemp.toInt();
        temp->name = in.read(iTemp);

        sTemp = in.read(5);
        iTemp = sTemp.toInt();
        temp->description = in.read(iTemp);

        sTemp = in.read(5);
        iTemp = sTemp.toInt();
        temp->pageTypes = in.read(iTemp);

        sTemp = in.read(5);
        iTemp = sTemp.toInt();
        temp->folderName = in.read(iTemp);

        /* read startDate */
        sTemp = in.read(2);
        iTemp = sTemp.toInt();
        temp->startDate.day = iTemp;
        sTemp = in.read(2);
        iTemp = sTemp.toInt();
        temp->startDate.month = iTemp;
        sTemp = in.read(4);
        iTemp = sTemp.toInt();
        temp->startDate.year = iTemp;

        /* read endDate */
        sTemp = in.read(2);
        iTemp = sTemp.toInt();
        temp->endDate.day = iTemp;
        sTemp = in.read(2);
        iTemp = sTemp.toInt();
        temp->endDate.month = iTemp;
        sTemp = in.read(4);
        iTemp = sTemp.toInt();
        temp->endDate.year = iTemp;

        /* read timePeriod */
        sTemp = in.read(2);
        iTemp = sTemp.toInt();
        temp->timePeriod.day = iTemp;
        sTemp = in.read(2);
        iTemp = sTemp.toInt();
        temp->timePeriod.month = iTemp;
        sTemp = in.read(4);
        iTemp = sTemp.toInt();
        temp->timePeriod.year = iTemp;
        //QByteArray line = file.readLine();
        //process_line(line);

        QString check = in.read(1);
        if(check != '\n')
            return -1;

        _album.add(*temp);
    }
    file.close();
    delete(temp);
    return 0;
}


/*!
 * \brief c_file::loadList, Loads a list of albums to memory
 * \param _head pointer to the start of the album list where the list will be saved
 * \return 0 if no error occurs, -1 if a error occurs
 */
int c_file::loadList(c_noAlbum *_head){
    QString filePath = rootPath+'/'+albumFiles+".txt";
    //std::fstream  outfile;
    QFile file(filePath);
    if(!file.open(QIODevice::ReadWrite | QIODevice::Text|QIODevice::Append))
        return -1;

    QTextStream in(&file);
    //in.setCodec("UTF-8");

    file.seek(0);
    while (!file.atEnd()) {

        QString sTemp = file.read(5);
        int iTemp = sTemp.toInt();
        _head->name = file.read(iTemp);

        sTemp = file.read(5);
        iTemp = sTemp.toInt();
        _head->description = file.read(iTemp);

        sTemp = file.read(5);
        iTemp = sTemp.toInt();
        _head->pageTypes = file.read(iTemp);

        sTemp = file.read(5);
        iTemp = sTemp.toInt();
        _head->folderName = file.read(iTemp);

        /* read startDate */
        sTemp = file.read(2);
        iTemp = sTemp.toInt();
        _head->startDate.day = iTemp;
        sTemp = file.read(2);
        iTemp = sTemp.toInt();
        _head->startDate.month = iTemp;
        sTemp = file.read(4);
        iTemp = sTemp.toInt();
        _head->startDate.year = iTemp;

        /* read endDate */
        sTemp = file.read(2);
        iTemp = sTemp.toInt();
        _head->endDate.day = iTemp;
        sTemp = file.read(2);
        iTemp = sTemp.toInt();
        _head->endDate.month = iTemp;
        sTemp = file.read(4);
        iTemp = sTemp.toInt();
        _head->endDate.year = iTemp;

        /* read timePeriod */
        sTemp = file.read(2);
        iTemp = sTemp.toInt();
        _head->timePeriod.day = iTemp;
        sTemp = file.read(2);
        iTemp = sTemp.toInt();
        _head->timePeriod.month = iTemp;
        sTemp = file.read(4);
        iTemp = sTemp.toInt();
        _head->timePeriod.year = iTemp;

        QString check = file.read(1);
        if(check != '\n')
            return -1;
        //QByteArray line = file.readLine();
        //process_line(line);
    }
    file.close();
    return 0;
}


/*!
 * \brief c_file::saveList, Saves a list of albums to a file
 * \param _head pointer to the start of the album list
 * \return  0 if no error occurs, -1 if a error occurs
 */
int c_file::saveList(c_noAlbum *_head){

    QString filePath = rootPath+'/'+albumFiles+".txt";
    //std::fstream  outfile;
    QFile file(filePath);
   // if(!file.remove())  //TODO, make copy of file and recovery of copy if original is corrupted.
    //    return -1;
    if(!file.open(QIODevice::ReadWrite | QIODevice::Text | QIODevice::Truncate))
        return -1;

    QTextStream out(&file);
    //out.setCodec("UTF-8");

    file.seek(0);
    c_noAlbum *temp = _head;
    while(temp != NULL){

        out << qSetFieldWidth(5) << temp->name.size()<< qSetFieldWidth(0) << temp->name
            << qSetFieldWidth(5) << temp->description.size() << qSetFieldWidth(0) << temp->description
            << qSetFieldWidth(5) << temp->pageTypes.size() << qSetFieldWidth(0) << temp->pageTypes
            << qSetFieldWidth(5) << temp->folderName.size() << qSetFieldWidth(0) << temp->folderName
            << qSetFieldWidth(2) << temp->startDate.day << temp->startDate.month << qSetFieldWidth(4) << temp->startDate.year
            << qSetFieldWidth(2) << temp->endDate.day << temp->endDate.month << qSetFieldWidth(4) << temp->endDate.year
            << qSetFieldWidth(2) << temp->timePeriod.day << temp->timePeriod.month << qSetFieldWidth(4) << temp->timePeriod.year
            << qSetFieldWidth(0) <<'\n';
        temp = temp->next;
    }
    file.close();
    return 0;
}


/*!
 * \brief c_file::loadList, Loads a list of pages to memory
 * \param _path QString path to the file from which to load from
 * \param _head pointer to the start of the page list where the list will be saved
 * \return
 */
int c_file::loadList(c_listPage &_list){
    QString albumName = _list.albumName;
    QString filePath = rootPath+'/'+albumName+".txt";
    //std::fstream  outfile;
    QFile file(filePath);
    if(!file.open(QIODevice::ReadWrite | QIODevice::Text | QIODevice::Text |QIODevice::Append))
        return -1;

    QTextStream in(&file);
    //in.setCodec("UTF-16");

    file.seek(0);
    c_noPage *temp = new(c_noPage);
    while (!in.atEnd()) {
        QString sTemp = in.read(5);
        int iTemp = sTemp.toInt();
        temp->name = in.read(iTemp);

        sTemp = in.read(5);
        iTemp = sTemp.toInt();
        temp->type = in.read(iTemp);

        /* read date */
        sTemp = in.read(2);
        iTemp = sTemp.toInt();
        temp->date.day = iTemp;
        sTemp = in.read(2);
        iTemp = sTemp.toInt();
        temp->date.month = iTemp;
        sTemp = in.read(4);
        iTemp = sTemp.toInt();
        temp->date.year = iTemp;

        sTemp = in.read(5);
        iTemp = sTemp.toInt();
        temp->partyType = in.read(iTemp);

        sTemp = in.read(5);
        iTemp = sTemp.toInt();
        temp->description = in.read(iTemp);

        /* read startDate */
        sTemp = in.read(2);
        iTemp = sTemp.toInt();
        temp->startDate.day = iTemp;
        sTemp = in.read(2);
        iTemp = sTemp.toInt();
        temp->startDate.month = iTemp;
        sTemp = in.read(4);
        iTemp = sTemp.toInt();
        temp->startDate.year = iTemp;

        /* read endDate */
        sTemp = in.read(2);
        iTemp = sTemp.toInt();
        temp->endDate.day = iTemp;
        sTemp = in.read(2);
        iTemp = sTemp.toInt();
        temp->endDate.month = iTemp;
        sTemp = in.read(4);
        iTemp = sTemp.toInt();
        temp->endDate.year = iTemp;


        //QByteArray line = file.readLine();
        //process_line(line);

        QString check = in.read(1);
        if(check != '\n')
            return -1;

        _list.add(*temp);
    }
    file.close();
    delete(temp);
    return 0;
}


/*!
 * \brief c_file::saveList, Saves a list of pages to a file
 * \param _list, list of pages to be saved
 * \return 0 if no error occurs, -1 if a error occurs
 */
int c_file::saveList(c_listPage *_list){
    QString fileName = convertString(_list->albumName);
    QString filePath = rootPath+'/'+fileName+".txt";
    //std::fstream  outfile;
    QFile file(filePath);
   // if(!file.remove())  //TODO, make copy of file and recovery of copy if original is corrupted.
    //    return -1;
    if(!file.open(QIODevice::ReadWrite | QIODevice::Text | QIODevice::Truncate))
        return -1;

    QTextStream out(&file);
    //out.setCodec("UTF-8");

    file.seek(0);
    c_noPage *temp = _list->head;
    while(temp != NULL){

        out << qSetFieldWidth(5) << temp->name.size()<< qSetFieldWidth(0) << temp->name
            << qSetFieldWidth(5) << temp->type.size() << qSetFieldWidth(0) << temp->type
            << qSetFieldWidth(2) << temp->date.day << temp->date.month << qSetFieldWidth(4) << temp->date.year
            << qSetFieldWidth(5) << temp->partyType.size() << qSetFieldWidth(0) << temp->partyType
            << qSetFieldWidth(5) << temp->description.size() << qSetFieldWidth(0) << temp->description
            << qSetFieldWidth(2) << temp->startDate.day << temp->startDate.month << qSetFieldWidth(4) << temp->startDate.year
            << qSetFieldWidth(2) << temp->endDate.day << temp->endDate.month << qSetFieldWidth(4) << temp->endDate.year
            << qSetFieldWidth(0) <<'\n';
        temp = temp->next;
    }
    file.close();
    return 0;
}

/*!
 * \brief c_file::loadList, loads a list of photos from a file
 * \param _list - list where to store the photos
 * \return  0 if no error occurs, -1 if a error occurs
 */
int c_file::loadList(c_listPhoto &_list){
    c_file t;
    QString pageName = t.convertString(_list.pageName);
    QString filePath = rootPath+'/'+pageName+".txt";
    //std::fstream  outfile;
    QFile file(filePath);
    if(!file.open(QIODevice::ReadWrite | QIODevice::Text | QIODevice::Text |QIODevice::Append))
        return -1;

    QTextStream in(&file);
    //in.setCodec("UTF-16");

    file.seek(0);
    c_noPhoto *temp = new(c_noPhoto);
    while (!in.atEnd()) {

        QString sTemp = in.read(5);
        int iTemp = sTemp.toInt();
        temp->name = in.read(iTemp);

        sTemp = in.read(5);
        iTemp = sTemp.toInt();
        temp->filePath = in.read(iTemp);
        //qDebug() << "teste: " << temp->filePath;

        /* read date */
        sTemp = in.read(2);
        iTemp = sTemp.toInt();
        temp->date.day = iTemp;
        sTemp = in.read(2);
        iTemp = sTemp.toInt();
        temp->date.month = iTemp;
        sTemp = in.read(4);
        iTemp = sTemp.toInt();
        temp->date.year = iTemp;

        sTemp = in.read(5);
        iTemp = sTemp.toInt();
        temp->resolution.x = iTemp;

        sTemp = in.read(5);
        iTemp = sTemp.toInt();
        temp->resolution.y = iTemp;



        //QByteArray line = file.readLine();
        //process_line(line);

        QString check = in.read(1);
        if(check != '\n')
            return -1;

        _list.add(*temp);
    }
    file.close();
    delete(temp);
    return 0;
    return 0;
}

/*!
 * \brief c_file::saveList, Saves a list of photos into a file
 * \param _list - list of photos to save
 * \return  0 if no error occurs, -1 if a error occurs
 */
int c_file::saveList(c_listPhoto *_list){
    QString fileName = convertString(_list->pageName);
    QString filePath = rootPath+'/'+fileName+".txt";
    //std::fstream  outfile;
    QFile file(filePath);
    // if(!file.remove())  //TODO, make copy of file and recovery of copy if original is corrupted.
    //    return -1;
    if(!file.open(QIODevice::ReadWrite | QIODevice::Text | QIODevice::Truncate))
        return -1;

    QTextStream out(&file);
    //out.setCodec("UTF-8");

    file.seek(0);
    c_noPhoto *temp = _list->head;
    while(temp != NULL){

        out << qSetFieldWidth(5) << temp->name.size()<< qSetFieldWidth(0) << temp->name
            << qSetFieldWidth(5) << temp->filePath.size() << qSetFieldWidth(0) << temp->filePath
            << qSetFieldWidth(2) << temp->date.day << temp->date.month << qSetFieldWidth(4) << temp->date.year
            << qSetFieldWidth(5) << temp->resolution.x << qSetFieldWidth(5) << temp->resolution.y
            << qSetFieldWidth(0) <<'\n';
        temp = temp->next;
    }
    file.close();

    return 0;
}

/*!
 * \brief c_file::loadList, loads a list of people from a file
 * \param _list - list where to store the people
 * \return  0 if no error occurs, -1 if a error occurs
 */
int c_file::loadList(c_listPeople &_list){
    c_file t;
    QString name = t.convertString(_list.name);
    QString filePath = rootPath+'/'+name+".txt";
    //std::fstream  outfile;
    QFile file(filePath);
    if(!file.open(QIODevice::ReadWrite | QIODevice::Text | QIODevice::Text |QIODevice::Append))
        return -1;

    QTextStream in(&file);
    //in.setCodec("UTF-16");

    file.seek(0);
    c_noPeople *temp = new(c_noPeople);
    while (!in.atEnd()) {

        QString sTemp = in.read(5);
        int iTemp = sTemp.toInt();
        temp->name = in.read(iTemp);

        sTemp = in.read(2);
        iTemp = sTemp.toInt();
        temp->birthday.day = iTemp;
        sTemp = in.read(2);
        iTemp = sTemp.toInt();
        temp->birthday.month = iTemp;
        sTemp = in.read(4);
        iTemp = sTemp.toInt();
        temp->birthday.year = iTemp;

        sTemp = in.read(5);
        iTemp = sTemp.toInt();
        temp->gender = iTemp;

        sTemp = in.read(5);
        iTemp = sTemp.toInt();
        temp->relation = iTemp;


        QString check = in.read(1);
        if(check != '\n')
            return -1;

        //_list.add(*temp);
    }
    file.close();
    delete(temp);
    return 0;
}

/*!
 * \brief c_file::saveList, Saves a list of people into a file
 * \param _list - list of people to save
 * \return  0 if no error occurs, -1 if a error occurs
 */
int c_file::saveList(c_listPeople *_list){
    QString fileName = convertString(_list->name);
    QString filePath = rootPath+'/'+fileName+".txt";
    //std::fstream  outfile;
    QFile file(filePath);
    // if(!file.remove())  //TODO, make copy of file and recovery of copy if original is corrupted.
    //    return -1;
    if(!file.open(QIODevice::ReadWrite | QIODevice::Text | QIODevice::Truncate))
        return -1;

    QTextStream out(&file);
    //out.setCodec("UTF-8");

    file.seek(0);
    c_noPeople *temp = _list->head;
    while(temp != NULL){

        out << qSetFieldWidth(5) << temp->name.size()<< qSetFieldWidth(0) << temp->name
            << qSetFieldWidth(2) << temp->birthday.day << temp->birthday.month << qSetFieldWidth(4) << temp->birthday.year
            << qSetFieldWidth(5) << temp->gender.size() << qSetFieldWidth(0) << temp->gender
            << qSetFieldWidth(5) << temp->relation.size() << qSetFieldWidth(0) << temp->relation
            << qSetFieldWidth(0) <<'\n';
        temp = temp->next;
    }
    file.close();

    return 0;
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
QString accent = "âêîôûŵŷäëïöüẅÿàèìòùẁỳáéíóúẃý";
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

QString accentTo = "aeiouwyaeiouwyaeiouwyaeiouwyc";

/*!
 * \brief c_listAlbum::convertString, converts some of the special characters of the QString
 * It converts spaces to "_", eliminates the accents (keeps the letters) and removes the rest of the special characters.
 * \param _toConvert - QString to convert
 * \return the converted QString
 */
QString c_file::convertString(QString _toConvert){
    QString name = _toConvert;

    name.replace(" ", "_");
    int size = accent.size();
    int nameSize = name.size();
    for(int k = 0; k <nameSize; k++){
        for(int i = 0; i < size; i++){
            if(name.at(k) == accent.at(i)){
                name.replace(k,1,accentTo.at(i));
            }

        }
    }

    name.remove(QRegExp("[-`~!@#$%^&*()—+=|:;<>«»,.?/{}\'\"\\\[\\\]\\\\]"));//[-`~!@#$%^&*()—+=|:;<>«»,.?/{}\'\"\\\[\\\]\\\\]

    return name;
}
