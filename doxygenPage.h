/* This file is just to create the Doxygen based reference manual main page*/

/*! \mainpage Main page
 
\section intro_sec Introduction

This is the reference manual for the software developed for the software engineering class of 2017-2018 MIEEC, UC project. Group 11.
Authors:
Luís Afonso 2013135191
Henrique Mendes 2013140431
João Pereira 2011164173

All the code follows a programming standard that should be delivered together with this reference manual.

The software intends to implement a photo management and visualization program. It must have albums, composed by pages which contain the photos. 
It will also have a list of people and allow to tag them into photos.


\section theManual_sec The manual 
The manual is generated with doxygen and provided in HTML and hyperlinked PDF. It's advisable to use the HTML format since it's more organized, with side bar and tabs.

Since we we're learning to use Doxygen from scratch there can be some doubled definitions that we couldn't "iron out".


\section theProjects Projects

The program has 2 projects, "prototipo_fotos" and "testFicheiros". One is the GUI and final program version and the other is just for developing the other 2 modules.
The first was used initially to develop the GUI standalone and then the rest of the modules we're integrated into it.
The second project has the source files for the other modules, used by the GUI project, and it's mainly for "Ficheiros" and "Gestão" unitary tests and integration of the 2.

\section architecture_sec Software Architecture
The software consists of 3 main modules:

- Graphical User Interface (GUI)
- Managing module (Gestão)
- File access module (Ficheiros)

These modules we're developed in QT and take advantage of some of the libraries, substituting some standard C++ ones like String for QString.

Bellow each module will be briefly explained, for more details consult the module and class pages.
 
 
 
\subsection GUI_section GUI
The GUI will allow the user to carry out all the functions and visualize the photos. 
The GUI is based in QT and is composed of 2 main classes, c_mainWindow and c_dialogoCriacao. We choose QT because it has a fast learning curve and we never made any GUI before.

The GUI is based on signals and slots.
There's a weird circular dependency between the classes which should be resolved in the future as more knowledge on QT is gained.
	
\subsection Gestao_section Gestão
This module is responsible for managing the list of data. 
All lists are connected lists and, although this is C++ and OOP it follows a philosophy more close to C. Unfortunately this was the extent of list knowledge we had.
Most methods between lists are very similar and are mostly copy paste from \b c_noAlbum and \b c_listAlbum with the needed changes to accommodate the different attributes.

Lists added to this module should be "inspired" by the methods in lists already implemented.

Current lists are:
- Album
- Page
- Photo
- People
 

 All lists are composed by 2 classes. The \b c_noXXXX and \b c_listXXXX, respectively the node with attributes and the class that manages the list of nodes.

 \b c_noXXXX minimum example
 \code{.cpp}
	class c_noXXXX{
	public:
		
		//attributes here

		c_noXXXX next;

		c_noXXXX(); //default constructor implemented
		c_noXXXX operator=(c_noXXXX _elementToAdd); //always needed
	};
\endcode
 
 
\b c_listXXXX example, all current lists have at least these methods
\code{.cpp}
	class c_listXXXX{
	private:
		c_noXXXX head;
		int elementIndex = 0;

		int checkIfExists(QString _attribute1); //or other attribute
		int createFolder(c_listXXXX _element); //each 
	public:


		c_listXXXX();
		~c_listXXXX();

		int loadList();
		int create(QString _attribute1, QString _attribute2, QString _attribute3);
		int add(c_noXXXX _elementToAdd);
		int modifyAttribute1(QString _attribute1, QString _newAttribute1);
		int remove(QString _attribute1); //remove node with attribute1 = _attribute1
		int removeAll();

		c_noXXXX search(QString _attribute1);

		int debugPrint(); //for debug purposes

		c_noXXXX openNode(QString _attribute1); //returns the Node address with _attribute1

		//These are very important for the GUI
		c_noXXXX getNode(int _n);
		void setElementIndex(int _n);
		c_noXXXX getNode();

	};
\endcode
 
 
 
\subsection Ficheiros_section Ficheiros
This module is responsible of saving and loading lists from files. It only takes care of the data, photos themselves are handled by the GUI.

Right now the module must know all types of nodes that it needs to save/load. It was the only way we saw to implement the function and then we didn't have time to make it mode independent.

The module is composed by a single class, \b c_file.
The module has "common" methods and then has 2 methods for each type of list, one to save and one to load. 

\code{.cpp}
	//"Common" methods
	int createFolder(QString _path);
	int changeFolderName(QString _path, QString _newPath);
	int removeFolder(QString _path);
	int createFile(QString _path);
\endcode

\code{.cpp}
	//Example of prototypes for save/load methods for album and page lists
	int loadList(c_listAlbum &_album);
	int saveList(c_noAlbum _head);

	int loadList(c_listPage &_list);
	int saveList(c_listPage _list);
\endcode

Note that you should try to follow the page methods <b> saveList() </b> - the album saveList is a leftover of early development.
 */